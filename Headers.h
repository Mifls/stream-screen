#ifndef UNTITLED_HEADERS_H
#define UNTITLED_HEADERS_H

#define GL_GLEXT_PROTOTYPES

#include <vector>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glext.h>
#include <glm/vec3.hpp> // glm::vec3
//#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp>
//#include <unistd.h>

#define Pi 3.14159265f
#define longPi 3.14159265358979323846L
#ifndef __SIZE_TYPE__
#define __SIZE_TYPE__ long unsigned int
typedef __SIZE_TYPE__ size_t;
#endif


#endif //UNTITLED_HEADERS_H
