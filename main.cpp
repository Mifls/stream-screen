#include "Headers.h"
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>

#include "Video/Outputs.h"
#include "Interface/Interface.h"
#include "Engine/Translation.h"
#include "Engine/Server.h"

// Global variables
std::vector<XDisplay*> displays;
XDisplay *primaryDisplay;
XWindow *RootWin;
XWindow *addScreenWin;
Server *server;
std::thread *server_control;

Strings *strings;
DisplayChoice *displayChoice;
NewDisplay *newDisplay;

std::vector<Translation*> Translations;

// Predefined functions
int Init();
int Terminate(int exitcode);

int main() {

    int exc = Init();
    if(exc) return exc;

    while (true) {
        while (XCheckTypedWindowEvent(RootWin->xDisplay->display, RootWin->window, ClientMessage, &RootWin->event)) {

            switch ( RootWin->event.type ) {
                case ClientMessage :
                    if(RootWin->event.xclient.data.l[0] == RootWin->wmDeleteMessage) Terminate(0);
            }
        }

        for (int i = 0; i < server->Clients.size(); ++i) {
            if (server->Clients[i]->isDisconnected()) {
                server->Clients[i]->clear();
                for (int j = i + 1; j < server->Clients.size(); ++j) {
                    server->Clients[j - 1] = server->Clients[j];
                }
                server->Clients.pop_back();
            }
        }

        for(auto display: displays) {
            display->updateScreen();
            display->update_root();
        }

        update_translations(&displays, &Translations);

        for (auto & Translation : Translations) {
            Translation->enabled = false;
        }
        for (auto & Client : server->Clients) {
            if(Client->translation != nullptr)
                Client->translation->enabled = Client->translation->isAvailable();

        }
        for (auto & Translation : Translations) {
            Translation->update();
        }

        displayChoice->update();
        displayChoice->redraw();

        //usleep(20000);
    }
}

int Init() {
    //Loading displays
    XInitThreads();
    printf("Loading displays...\n");
    if(DisplayLoad(&displays)) Terminate(1);
    primaryDisplay = displays[0];
    printf("Display %s has been selected as primary.\n",
           primaryDisplay->display_name);

    //Open a window in accordance with the golden ratio, in 75% of one side of the main screen
    primaryDisplay->updatePrimarySize();
    auto x = (float)primaryDisplay->primarySize.height * GoldenRatio;
    auto y = (float)primaryDisplay->primarySize.height;
    float scale = ScaleArea((float)primaryDisplay->primarySize.width * 0.75f,
                            (float)primaryDisplay->primarySize.height * 0.75f, x, y);
    RootWin = new XWindow(displays[0], (u_int32_t)(x*scale),
                          (u_int32_t)(y*scale), true);
    if (!RootWin->created) return 1;

    server = new Server(true);

    server_control = new std::thread([](Server *server){
        if(server != nullptr) {
            while(server->start(3425) != 0) { usleep(1e6); }
        }
    }, server);

    //setlocale(LC_ALL,setlocale(LC_CTYPE,NULL));

    Strings::init();
    //strings = new Strings(setlocale(LC_CTYPE,nullptr));

    addScreenWin = new XWindow(displays[0], (u_int32_t)(x*scale*0.3f),
                               (u_int32_t)(y*scale*0.3f), false);
    if (!addScreenWin->created) return 1;
    newDisplay = new NewDisplay(addScreenWin);
    newDisplay->update();

    displayChoice = new DisplayChoice(RootWin, displays, &Translations, server, newDisplay);
    displayChoice->update();

    return 0;
}

int Terminate(int exitcode){
    RootWin->close();
    size_t l = displays.size();
    for (size_t i = 0; i < l; ++i) {
        XCloseDisplay(displays[i]->display);
    }

    displayChoice->clear();
    server->terminate();
    if(server_control->joinable()) server_control->join();
    exit(exitcode);
}
