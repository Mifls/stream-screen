#ifndef OPENGL_LESSONS_UTILS_H
#define OPENGL_LESSONS_UTILS_H
#include <ctime>
#include "../Headers.h"

class Timer{
public:
    long int lastDiff;
    struct timespec t;
    Timer(){
        Reset();
    }
    long int countDiff(){
        lastDiff = t.tv_nsec + t.tv_sec*1e9;
        if(clock_gettime(CLOCK_MONOTONIC_RAW, &t))return 0;
        lastDiff = t.tv_nsec + t.tv_sec*1e9  - lastDiff;
        return lastDiff;
    }

    long int countDiff_no_reset(){
        lastDiff = t.tv_nsec + t.tv_sec*1e9;
        struct timespec _t;
        if(clock_gettime(CLOCK_MONOTONIC_RAW, &_t))return 0;
        lastDiff = _t.tv_nsec + _t.tv_sec*1e9  - lastDiff;
        return lastDiff;
    }

    void Reset(){
        lastDiff = 0;
        clock_gettime(CLOCK_MONOTONIC_RAW, &t);
    }
};

class FPS_counter{
public:
    int counter = 0;
    int frequency = 5;
    long int fpsTime = 0;
    int currentFPS = 0;
    void cutoff(long int diff_ns){
        fpsTime+=diff_ns;
        counter++;
        if(counter == frequency){
            currentFPS = ((1000000000.L / (double)(fpsTime / counter)));
            fpsTime = 0;
            counter = 0;
        }
    }
};

#endif //OPENGL_LESSONS_UTILS_H
