#ifndef OPENGL_LESSONS_TEXTURE_H
#define OPENGL_LESSONS_TEXTURE_H

#include <SOIL/SOIL.h>
#include <GL/gl.h>

GLuint LoadTexture(char* imgFile)
{
    int width, height;
    unsigned char* image = SOIL_load_image(imgFile, &width, &height, 0, SOIL_LOAD_RGBA);

    GLuint texture;
    glGenTextures(1, &texture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glBindTexture(GL_TEXTURE_2D, texture);

    float borderColor[] = { 1.0f, 0.0f, 0.0f, 1.0f };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_2D);

    SOIL_free_image_data(image);
    glBindTexture(GL_TEXTURE_2D, 0);
    return texture;
}

XImage* LoadImage(Display *display, Visual *visual, char* imgFile){
    int width, height, mul;
    unsigned char* image = SOIL_load_image(imgFile, &width, &height, 0, SOIL_LOAD_RGBA);
    mul = width * height * 4;
    unsigned char* data = new unsigned char[mul];
    std::memcpy(data, image, mul);
    SOIL_free_image_data(image);
    for (int i = 3; i < mul; i += 4) {
        data[i] = 255 - data[i];
    }
    auto *img = XCreateImage(display, visual, 32, ZPixmap, 0, (char*)(data), width, height, 32, width * 4);
    return img;
}

#endif