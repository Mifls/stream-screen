#ifndef OPENGL_LESSONS_TEXT_H
#define OPENGL_LESSONS_TEXT_H

#include "Shaders.h"
#include <freetype2/ft2build.h>
#include FT_FREETYPE_H
#include <iostream>

struct Character {
    GLuint     TextureID; // ID текстуры глифа
    glm::ivec2 Size;      // Размеры глифа
    glm::ivec2 Bearing;   // Смещение верхней левой точки глифа
    GLuint     Advance;   // Горизонтальное смещение до начала следующего глифа
};

class TextRenderer{
private:
    //Buffers
    GLuint VBO, VAO;

    FT_Library ft;
    FT_Face face;

    char*  filePath;
    size_t charCount;
    size_t size;
    Character *Characters;

    glm::mat4 projection;

public:

    TextRenderer(char*  filePath, size_t charCount, size_t size){
        this->filePath = filePath;
        this->charCount = charCount;
        this->Characters = new Character[charCount];
        this->size = size;
        this->projection = glm::ortho(0.0f, static_cast<GLfloat>(800),
                                      0.0f, static_cast<GLfloat>(600));
        InitBuffers();
        LoadFont();
    }

    size_t getCharCount(){ return this->charCount;}
    void setProjection(glm::mat4 projection){this->projection = projection;}

    void InitBuffers(){
        //Init buffers
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    void LoadFont(){
        if (FT_Init_FreeType(&ft))
            printf("ERROR::FREETYPE: Could not init FreeType Library\n");

        if (FT_New_Face(ft, this->filePath, 0, &face))
            printf("ERROR::FREETYPE: Failed to load font\n");

        if (FT_Select_Charmap(face, ft_encoding_unicode))
            printf("ERROR::FREETYPE: Failed to set charmap\n");


        FT_Set_Pixel_Sizes(face, 0, this->size);


        glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction

        for (GLuint c = 0; c < this->charCount; c++)
        {
            // Load character glyph
            if (FT_Load_Char(face, c, FT_LOAD_RENDER))
            {
                printf("ERROR::FREETYTPE: Failed to load Glyph\n");
                continue;
            }
            // Generate texture
            GLuint texture;
            glGenTextures(1, &texture);
            glBindTexture(GL_TEXTURE_2D, texture);
            glTexImage2D(
                    GL_TEXTURE_2D,
                    0,
                    GL_RED,
                    face->glyph->bitmap.width,
                    face->glyph->bitmap.rows,
                    0,
                    GL_RED,
                    GL_UNSIGNED_BYTE,
                    face->glyph->bitmap.buffer
            );
            // Set texture options
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            // Now store character for later use
            Characters[c] = {
                    texture,
                    glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                    glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
                    (GLuint)face->glyph->advance.x
            };
        }


        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

        FT_Done_Face(face);   // Завершение работы с шрифтом face
        FT_Done_FreeType(ft); // Завершение работы FreeType
    }

    void RenderText(ShaderProgram *s, std::u16string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color){
        // Activate corresponding render state
        s->Use();

        glUniformMatrix4fv(glGetUniformLocation(s->ProgramID, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

        glUniform3f(glGetUniformLocation(s->ProgramID, "textColor"), color.x, color.y, color.z);
        glActiveTexture(GL_TEXTURE0);
        glBindVertexArray(this->VAO);

        // Iterate through all characters
        std::u16string::const_iterator c;
        for (c = text.begin(); c != text.end(); c++)
        {
            Character ch = Characters[*c];

            GLfloat xPos = x + ch.Bearing.x * scale;
            GLfloat yPos = y - (ch.Size.y - ch.Bearing.y) * scale;

            GLfloat w = ch.Size.x * scale;
            GLfloat h = ch.Size.y * scale;
            // Update VBO for each character
            GLfloat vertices[6][4] = {
                    { xPos, yPos + h, 0.0f, 0.0f },
                    { xPos, yPos, 0.0f, 1.0f },
                    { xPos + w, yPos, 1.0f, 1.0f },
                    { xPos, yPos + h, 0.0f, 0.0f },
                    { xPos + w, yPos, 1.0f, 1.0f },
                    { xPos + w, yPos + h, 1.0f, 0.0f }
            };
            // Render glyph texture over quad
            glBindTexture(GL_TEXTURE_2D, ch.TextureID);
            // Update content of VBO memory
            glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            // Render quad
            glDrawArrays(GL_TRIANGLES, 0, 6);
            // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
            x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
        }
        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    GLfloat lineWidth(std::u16string text, GLfloat scale){
        GLfloat x = 0;
        std::u16string::const_iterator c;
        for (c = text.begin(); c != text.end(); c++)
        {
            x += (Characters[*c].Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
        }
        return x;
    }

    GLfloat lineHeight(std::u16string text, GLfloat scale){
        GLfloat x = 0;
        std::u16string::const_iterator c;
        for (c = text.begin(); c != text.end(); c++)
        {
            x = std::max(x, (Characters[*c].Size.y) * scale); // Bitshift by 6 to get value in pixels (2^6 = 64)
        }
        return x;
    }

    void clear(){
        delete [] Characters;
        glDeleteVertexArrays(1, &this->VAO);
        glDeleteBuffers(1, &this->VBO);
    }
};

#endif //OPENGL_LESSONS_TEXT_H
