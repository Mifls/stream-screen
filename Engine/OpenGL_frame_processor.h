#ifndef STREAM_SCREEN_OPENGL_FRAME_PROCESSOR_H
#define STREAM_SCREEN_OPENGL_FRAME_PROCESSOR_H

class OpenGLFrameProcessor{
private:
    XDisplay *xDisplay;

    //Buffers
    GLuint VAO;
    GLuint VBO;
    GLuint EBO;

    GLuint width, height;

    GLfloat *vertices;
    GLuint *indices;

    glm::mat4 projection;

public:
    struct dataBlock{
        GLint locParam;
        GLchar *name;
        GLchar *data;
        GLint format;
        GLuint width, height;
        GLuint texture;
    };

    ShaderProgram *shader;
    XWindow *xWindow;

    std::vector<dataBlock> dataBlocks;

    OpenGLFrameProcessor(XDisplay *xDisplay, GLuint width, GLuint height){
        this->xDisplay = xDisplay;
        this->width = width;
        this->height = height;

        xWindow = new XWindow(xDisplay, width, height, false);

        glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);

        glGenVertexArrays(1, &this->VAO);
        glGenBuffers(1, &this->EBO);
        glGenBuffers(1, &this->VBO);


        vertices = new GLfloat[8];
        updateVertices();

        glBindVertexArray(this->VAO);

        glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
        glVertexAttribPointer(0, 2, GL_FLOAT,GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(0);

        indices = new GLuint[6]{
                0, 1, 2,
                0, 2, 3};

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*6, this->indices, GL_STATIC_DRAW);

        // Отвязываем VAO
        glBindVertexArray(0);
    }

    GLint compileShader(char * vertex_file_path, char * fragment_file_path){
        glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);
        shader = new ShaderProgram(vertex_file_path, fragment_file_path);
        shader->loadFromFile();
    }

    void updateVertices(){

        vertices[0] = 0;
        vertices[1] = 0;
        vertices[2] = width*1.f;
        vertices[3] = 0;
        vertices[4] = width*1.f;
        vertices[5] = height*1.f;
        vertices[6] = 0;
        vertices[7] = height*1.f;

        glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);
        glBindVertexArray(this->VAO);
        glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*8, this->vertices, GL_STATIC_DRAW);
        glBindVertexArray(0);

        XResizeWindow(this->xWindow->xDisplay->display, this->xWindow->window, width, height);

        this->xWindow->updateAttribs();
        glViewport(0, 0, width, height);
        projection = glm::ortho(0.0f, width * 1.f,
                                0.0f, height * 1.f);
    }

    GLint addData(GLchar *name, GLchar *data, GLint internalFormat, GLint format, GLuint width, GLuint height){
        if(dataBlocks.size() < 16){
            glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);
            dataBlock x;
            x.name = name;
            x.data = data;
            x.format = format;
            x.width = width;
            x.height = height;
            x.locParam = glGetUniformLocation(shader->ProgramID, name);

            glGenTextures(1, &x.texture);
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            glBindTexture(GL_TEXTURE_2D, x.texture);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, 0);

            dataBlocks.emplace_back(x);

            return dataBlocks.size()-1;
        }
        else return -1;
    }

    GLint changeData(GLuint index, GLchar *data, GLint internalFormat, GLint format, GLuint width, GLuint height){
        if(dataBlocks.size() > index){
            glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);
            dataBlocks[index].data = data;
            dataBlocks[index].format = format;
            dataBlocks[index].width = width;
            dataBlocks[index].height = height;
            dataBlocks[index].locParam = glGetUniformLocation(shader->ProgramID, dataBlocks[index].name);

            glBindTexture(GL_TEXTURE_2D, dataBlocks[index].texture);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, 0);

            return 0;
        }
        else return -1;
    }

    void setSize(GLuint width, GLuint height){
        if((this->width != width)||(this->height != height)) {
            this->width = width;
            this->height = height;
            updateVertices();
        }
    }

    void render(){
        glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);
        glClear(GL_COLOR_BUFFER_BIT);
        shader->Use();
        glUniformMatrix4fv(glGetUniformLocation(shader->ProgramID, "projection"), 1,
                           GL_FALSE, glm::value_ptr(projection));
        //glUniform1ui(locParam, shaderParam);
        //glBindTexture(GL_TEXTURE_2D,this->texture);

        for (int i = 0; i < dataBlocks.size(); ++i) {
            glActiveTexture(GL_TEXTURE0 + i);
            glBindTexture(GL_TEXTURE_2D, dataBlocks[i].texture);
            glUniform1i(glGetUniformLocation(shader->ProgramID, dataBlocks[i].name), i);
        }

        glBindVertexArray(this->VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);

        glFlush();
        //glXSwapBuffers(this->xWindow->xDisplay->display, this->xWindow->window);
    }

    void getData(GLuint* data, GLint format){
        glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);
        glReadPixels(0, 0, width, height, format, GL_UNSIGNED_BYTE, data);
    }

};

#endif //STREAM_SCREEN_OPENGL_FRAME_PROCESSOR_H
