#ifndef STREAM_SCREEN_AUTHORIZATION_H
#define STREAM_SCREEN_AUTHORIZATION_H

#include <gcrypt.h>

class Diffie_Hellman{
private:
    gcry_mpi_t base{};
    gcry_mpi_t privkey{};
    gcry_mpi_t pubkey;
    gcry_mpi_t p;
    size_t keylen;
    u_int8_t *key = nullptr;
public:

    Diffie_Hellman(){
        gcry_check_version (GCRYPT_VERSION);
        u_int32_t num = 1;
        p = gcry_mpi_new(0);
        pubkey = gcry_mpi_new(0);

        // переводим mybignum в формат gcrypt
        size_t scanned = 0;
        gcry_mpi_scan(&p, GCRYMPI_FMT_USG, &num, sizeof(u_int32_t), &scanned);

        gcry_mpi_mul_2exp (p, p, 2203);
        gcry_mpi_sub_ui (p, p, 1);

        auto *buffer = (unsigned char*)gcry_random_bytes_secure (65, GCRY_STRONG_RANDOM);
        gcry_mpi_scan(&privkey, GCRYMPI_FMT_USG, buffer, 64, &scanned);
        buffer[64] = 101 + buffer[64]%100;
        gcry_mpi_scan(&base, GCRYMPI_FMT_USG, buffer + 64, 1, &scanned);
        gcry_free(buffer);
        gcry_mpi_powm(pubkey, base, privkey, p);
    }

    Diffie_Hellman(u_int8_t _g){
        gcry_check_version (GCRYPT_VERSION);
        u_int32_t num = 1;
        p = gcry_mpi_new(0);
        pubkey = gcry_mpi_new(0);

        // переводим mybignum в формат gcrypt
        size_t scanned = 0;
        gcry_mpi_scan(&p, GCRYMPI_FMT_USG, &num, sizeof(u_int32_t), &scanned);

        gcry_mpi_mul_2exp (p, p, 2203);
        gcry_mpi_sub_ui (p, p, 1);

        auto *buffer = (unsigned char*)gcry_random_bytes_secure (64, GCRY_STRONG_RANDOM);
        gcry_mpi_scan(&privkey, GCRYMPI_FMT_USG, buffer, 64, &scanned);
        gcry_free(buffer);
        gcry_mpi_scan(&base, GCRYMPI_FMT_USG, &_g, 1, &scanned);
        gcry_mpi_powm(pubkey, base, privkey, p);
    }

    void countKey(unsigned char* buffer, u_int32_t len){
        size_t scanned = 0;
        gcry_mpi_t _key;
        gcry_mpi_scan(&_key, GCRYMPI_FMT_USG, buffer, len, &scanned);
        gcry_mpi_powm(_key, _key, privkey, p);
        len = gcry_mpi_get_nbits(_key);
        keylen = len / 8;
        if(len % 8 > 0) keylen++;
        key = (u_int8_t*)gcry_malloc_secure(keylen);
        gcry_mpi_print(GCRYMPI_FMT_USG, key, keylen, nullptr, _key);
        for (int i = 0; i < 64; ++i) {
            printf("%c", key[i]);
        }
        gcry_mpi_release(_key);
    }

    u_int32_t encrypt(void *buffer, void *out, u_int32_t len){
        gcry_error_t gcryError;
        u_int32_t ret;
        gcry_cipher_hd_t hd;
        gcryError = gcry_cipher_open (&hd, GCRY_CIPHER_ARCFOUR, GCRY_CIPHER_MODE_STREAM, GCRY_CIPHER_SECURE);
        if (gcryError) {
            printf("gcry_cipher_open failed:  %s/%s\n",
                   gcry_strsource(gcryError), gcry_strerror(gcryError));
            return -1;
        }
        gcry_cipher_setkey (hd, key, keylen);
        ret = gcry_cipher_encrypt (hd, out, len, buffer, len);
        gcry_cipher_close (hd);
        return ret;
    }

    u_int32_t decrypt(void *buffer, void *out, u_int32_t len){
        u_int32_t ret;
        gcry_cipher_hd_t hd;
        gcry_cipher_open (&hd, GCRY_CIPHER_ARCFOUR, GCRY_CIPHER_MODE_STREAM, GCRY_CIPHER_SECURE);
        gcry_cipher_setkey (hd, key, keylen);
        ret = gcry_cipher_decrypt (hd, out, len, buffer, len);
        gcry_cipher_close (hd);
        return ret;
    }

    u_int8_t getBase(){
        u_int16_t ret;
        gcry_mpi_print(GCRYMPI_FMT_USG, (u_int8_t*)(&ret), 2, NULL, base);
        return ret;
    }

    u_int16_t getPubkey(unsigned char **buffer){
        size_t ret = gcry_mpi_get_nbits(pubkey);
        u_int16_t len = ret / 8;
        if(ret % 8 > 0) len++;
        *buffer = new unsigned char[len];
        gcry_mpi_print(GCRYMPI_FMT_USG, *buffer, len, &ret, pubkey);
        return len;
    }

    void clear(){
        gcry_mpi_release(base);
        gcry_mpi_release(privkey);
        gcry_mpi_release(pubkey);
        gcry_mpi_release(p);
        if(key != nullptr) gcry_free(key);
    }
};


#endif //STREAM_SCREEN_AUTHORIZATION_H
