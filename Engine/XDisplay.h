#ifndef STREAM_SCREEN_XDISPLAY_H
#define STREAM_SCREEN_XDISPLAY_H

#include "../Headers.h"
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>
#include <dirent.h>


class XDisplay{
public:
    char display_name[64];
    Display* display;
    Window root;
    XWindowAttributes rootAttribs;

    int defaultScreenId;
    XVisualInfo* visualInfo;
    Screen *screen;

    struct {size_t width, height;} size;

    std::vector<XRROutputInfo*> outputs;
    std::vector<RROutput> freeOutputs;
    std::vector<std::pair<XRRCrtcInfo*, int>> crtcs;

    struct {size_t width, height;} primarySize;

    int Init(){
        this->display = XOpenDisplay(this->display_name);
        if (this->display == nullptr) return 1;

        updateScreen();
        update_root();

        GLint glxAttribs[] = {
                GLX_RGBA,
                GLX_DOUBLEBUFFER,
                GLX_DEPTH_SIZE,     24,
                GLX_STENCIL_SIZE,   8,
                GLX_RED_SIZE,       8,
                GLX_GREEN_SIZE,     8,
                GLX_BLUE_SIZE,      8,
                GLX_SAMPLE_BUFFERS, 0,
                GLX_SAMPLES,        0,
                None
        };

        this->visualInfo = glXChooseVisual(this->display, this->defaultScreenId, glxAttribs);
        if (this->visualInfo == nullptr) {
            printf("Could not create correct visual window.\n");
            XCloseDisplay(this->display);
            return 1;
        }

        crtcs.clear();

        return 0;
    }

    void updateOutputs(){
        XRRScreenResources *screen_res;
        XRROutputInfo *info;
        XRRCrtcInfo *crtc_info;

        for (auto i: crtcs) XRRFreeCrtcInfo(i.first);
        crtcs.clear();

        for (auto i: outputs) XRRFreeOutputInfo(i);
        outputs.clear();

        freeOutputs.clear();

        screen_res = XRRGetScreenResources(this->display, DefaultRootWindow(this->display));

        for (int iscres = screen_res->noutput - 1; iscres >= 0; --iscres) {

            info = XRRGetOutputInfo (this->display, screen_res, screen_res->outputs[iscres]);
            if (info->connection == RR_Connected){
                outputs.emplace_back(info);
                for (int icrtc = info->ncrtc - 1; icrtc >= 0; --icrtc) {
                    crtc_info = XRRGetCrtcInfo (this->display, screen_res, info->crtcs[icrtc]);
                    if(crtc_info->noutput > 0) crtcs.emplace_back(std::make_pair(crtc_info, outputs.size() - 1));
                    else XRRFreeCrtcInfo(crtc_info);
                }
            }
            else {
                XRRFreeOutputInfo (info);
                freeOutputs.emplace_back(screen_res->outputs[iscres]);
            }
        }
        XRRFreeScreenResources(screen_res);
    }

    int updatePrimarySize(){

        XRRScreenResources *screen_res;
        XRROutputInfo *out_info;
        XRRCrtcInfo *crtc_info;

        screen_res = XRRGetScreenResources(this->display, this->root);
        out_info = XRRGetOutputInfo (this->display, screen_res, XRRGetOutputPrimary(this->display, this->root));
        crtc_info = XRRGetCrtcInfo (this->display, screen_res, out_info->crtc);
        this->primarySize.width = crtc_info->width;
        this->primarySize.height = crtc_info->height;

        XRRFreeCrtcInfo(crtc_info);
        XRRFreeOutputInfo(out_info);
        XRRFreeScreenResources(screen_res);

        return 0;
    }

    void update_root() {
        XGetWindowAttributes(display, root, &rootAttribs);
    }

    void updateScreen(){
        screen = DefaultScreenOfDisplay(this->display);
        root = RootWindowOfScreen(this->screen);
        defaultScreenId = DefaultScreen(this->display);
    }

};

bool comp (XDisplay* i,XDisplay* j) { return strcmp(i->display_name,j->display_name)<0; }

int DisplayLoad(std::vector<XDisplay*> *displays){
    DIR* dir = opendir("/tmp/.X11-unix");

    if (dir != nullptr) {
        struct dirent *p;
        while ((p = readdir(dir)) != nullptr) {
            if (p->d_name[0] != 'X')
                continue;

            XDisplay* dsp = new XDisplay();
            displays->emplace_back(dsp);
            strcpy(dsp->display_name, ":");
            strcat(dsp->display_name, p->d_name + 1);

            if (dsp->Init() == 0) {

                int count = XScreenCount(dsp->display);
                printf("Display %s has %d screens (default %d)\n",
                       dsp->display_name, count, dsp->defaultScreenId);

                for (uint32_t i = 0; i < count; i++)
                    printf(" %d: %dx%d\n",
                           i, XDisplayWidth(dsp->display, i), XDisplayHeight(dsp->display, i));
            }
            else{
                displays->pop_back();
            }

        }
        closedir(dir);

        std::sort(displays->begin(), displays->end(), comp);
    }
    else {
        printf("Error: can't open /tmp/.X11-unix directory!");
        return 1;
    }
    if(displays->empty()){
        printf("Error: can't open any display!");
        return 1;
    }
    return 0;
}


#endif //STREAM_SCREEN_XDISPLAY_H