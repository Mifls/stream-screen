#ifndef STREAM_SCREEN_SERVER_H
#define STREAM_SCREEN_SERVER_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <thread>
#include <queue>
#include <csignal>
#include <unistd.h>
#include "Server/Authorization.h"
#include "Translation.h"
#include "../Video/Outputs.h"

#define ENCRYPTED


int sendEncrypted(Diffie_Hellman *dh, int sockfd, void *buffer, u_int16_t len){
#ifdef ENCRYPTED
    auto encryptedBuffer = gcry_malloc(len);
    dh->encrypt(buffer, encryptedBuffer, len);
    int ret = send(sockfd, encryptedBuffer, len, 0);
    gcry_free(encryptedBuffer);
#else
    int ret = send(sockfd, buffer, len, 0);
#endif
    return ret;
}

int recvEncrypted(Diffie_Hellman *dh, int sockfd, void *buffer, u_int16_t len){
#ifdef ENCRYPTED
    auto encryptedBuffer = gcry_malloc(len);
    int ret = recv(sockfd, encryptedBuffer, len, 0);
    dh->decrypt(encryptedBuffer, buffer, len);
    gcry_free(encryptedBuffer);
#else
    int ret = recv(sockfd, buffer, len, 0);
#endif

    return ret;
}

class client{
public:
    struct packet{
        uint8_t *data;
        uint32_t data_size;
        uint64_t number;
    };

    Timer timer;

private:
    Diffie_Hellman *dh;
    std::thread *connectionThread = nullptr;
    std::thread *mainThread = nullptr;
    int sock_fd;
    char format[16] = "mpeg";
    MpegConverter *converter = nullptr;
    std::queue<packet> pkt;
    uint64_t lastPacket = -1;
    uint32_t pause = 5000;
    uint64_t missedPackets = 0;
    bool authorized = false;
    bool disconnected = false;

public:
    char name[256] = "Device";
    Translation *translation = nullptr;

    client(int sock_fd){
        this->sock_fd = sock_fd;
        dh = new Diffie_Hellman();
    }

    void pairing() {
#ifdef ENCRYPTED

        u_int16_t base = dh->getBase();
            if(send(sock_fd, &base, sizeof(u_int16_t), 0) < 0){
                errno = 0;
                close(sock_fd);
                disconnected = true;
                printf("Client %d disconnected\n", sock_fd);
            }

            u_int8_t *buffer;
            u_int16_t len = dh->getPubkey(&buffer);
            if(send(sock_fd, &len, sizeof(u_int16_t), 0) < 0){
                errno = 0;
                close(sock_fd);
                disconnected = true;
                printf("Client %d disconnected\n", sock_fd);
            }
            if(send(sock_fd, buffer, sizeof(char) * len, 0) < 0){
                errno = 0;
                close(sock_fd);
                disconnected = true;
                printf("Client %d disconnected\n", sock_fd);
            }
            delete buffer;

            int bytes_read = recv(sock_fd, &len, sizeof(u_int16_t), 0);
            if(bytes_read <= 0) return;

            buffer = new u_int8_t[len];
            bytes_read = recv(sock_fd, buffer, sizeof(u_int8_t)*len, 0);
            if(bytes_read <= 0) return;
            dh->countKey(buffer, len);
            delete buffer;
#endif
        if(recvEncrypted(dh, sock_fd, name, 256) < 0){
            errno = 0;
            close(sock_fd);
            disconnected = true;
            printf("Client %d disconnected\n", sock_fd);
        }
        authorized = true;
    }

    void start(char *_format, Translation *_translation) {
        strcpy(format, _format);
        translation = _translation;
        connectionThread = new std::thread([](client *cl){
#ifdef ENCRYPTED
#define DH cl->dh
#else
#define DH nullptr
#endif
            if(sendEncrypted(DH, cl->sock_fd, cl->format, 16) < 0){
                errno = 0;
                cl->terminate();
                return;
            }
            const uint32_t maxBufferSize = 32767;
            uint32_t answer;
            uint16_t answer16;
            bool working = true;
            while(working)
            {
                while (!cl->pkt.empty()) {
                    uint64_t packetRcv;
                    int bytes_read = recvEncrypted(DH, cl->sock_fd, &packetRcv, sizeof(uint64_t));
                    if (bytes_read <= 0) {
                        working = false;
                        break;
                    }
                    cl->timer.Reset();
                    //printf("Client %d: %lu %lu\n", cl->sockfd, cl->pkt.front().number, packetRcv);

                    if (cl->pkt.front().number < packetRcv + cl->missedPackets) {
                        cl->lastPacket = cl->pkt.front().number - cl->missedPackets;
                        if(cl->pkt.front().data != nullptr) delete cl->pkt.front().data;
                        cl->pkt.pop();
                    } else if (cl->pkt.front().number == packetRcv + cl->missedPackets) {
                        uint32_t data_size[2];
                        data_size[0] = cl->pkt.front().data_size;
                        uint8_t *data = cl->pkt.front().data;
                        uint32_t parts = data_size[0] / maxBufferSize;
                        uint16_t *packet_size;
                        if (data_size[0] % maxBufferSize > 0) parts++;
                        data_size[1] = parts;

                        if (data_size[0] < 0) {
                            printf("Client %d: Invalid package.\n", cl->sock_fd);
                            cl->missedPackets++;
                            cl->lastPacket = cl->pkt.front().number - cl->missedPackets;
                            if(cl->pkt.front().data != nullptr) delete cl->pkt.front().data;
                            cl->pkt.pop();
                        } else {

                            if (sendEncrypted(DH, cl->sock_fd, data_size, sizeof(uint32_t) * 2) < 0) {
                                errno = 0;
                                break;
                            }
                            packet_size = new uint16_t[parts];
                            for (int i = 0; i < parts; ++i) {
                                packet_size[i] = std::min((uint32_t) maxBufferSize, data_size[0]);
                                data_size[0] -= packet_size[i];
                            }

                            if (sendEncrypted(DH, cl->sock_fd, packet_size, sizeof(uint16_t) * parts) < 0) {
                                errno = 0;
                                break;
                            }

                            for (int i = 0; i < parts; ++i) {
                                if (sendEncrypted(DH, cl->sock_fd, data, sizeof(uint8_t) * packet_size[i]) < 0) {
                                    errno = 0;
                                    break;
                                }

                                bytes_read = recvEncrypted(DH, cl->sock_fd, &answer16, sizeof(uint16_t));
                                if (bytes_read <= 0) {
                                    working = false;
                                    break;
                                }
                                cl->timer.Reset();
                                if (packet_size[i] != answer16) {
                                    i--;
                                } else {
                                    data += packet_size[i];
                                }

                            }
                            if (packet_size != nullptr) delete (packet_size);

                            bytes_read = recvEncrypted(DH, cl->sock_fd, &answer, sizeof(uint32_t));
                            if (bytes_read <= 0) {
                                working = false;
                                break;
                            }
                            cl->timer.Reset();
                            if (cl->pkt.front().data_size == answer) {
                                cl->lastPacket = cl->pkt.front().number - cl->missedPackets;
                                if(cl->pkt.front().data != nullptr) delete cl->pkt.front().data;
                                cl->pkt.pop();
                            }
                        }
                    }
                    else if (cl->pkt.front().number > packetRcv + cl->missedPackets) {
                        cl->missedPackets = cl->pkt.front().number - packetRcv;
                    }
                }

                usleep(cl->pause);

            }
            cl->terminate();
        }, this);

        timer.Reset();

        mainThread = new std::thread([](client *cl){
            uint8_t *data;
            size_t data_size;
            while(true) {
                if(cl->connectionThread->joinable()) {
                    if(cl->timer.countDiff_no_reset() > 1e10) {
                        cl->terminate();
                        break;
                    }
                    if (cl->pkt.empty()) {
                        if (cl->converter == nullptr) {
                            if (strcmp(cl->format, "mpeg") == 0) {
                                cl->converter = new MpegConverter(cl->translation->capture->xDisplay,
                                                                  cl->translation->capture->image);
                            }
                        }
                        cl->converter->update();

                        while (cl->converter->getPacket(&data, &data_size)) {

                            if (!data_size)
                                break;

                            packet t{};
                            t.data_size = data_size;
                            t.data = new uint8_t[data_size];
                            std::memcpy(t.data, data, data_size);

                            t.number = cl->lastPacket + 1;
                            cl->pkt.push(t);
                        }
                    }
                    else {
                        usleep(cl->pause);
                    }
                }
                else {
                    errno = 0;
                    cl->terminate();
                    break;
                }
            }

        }, this);
    }

    bool isAuthorized() { return authorized; }

    bool isDisconnected() { return disconnected; }

    void terminate() {
        if(!disconnected) {
            shutdown(sock_fd, SHUT_RDWR);
            close(sock_fd);
            while (!pkt.empty()) { pkt.pop(); }
            disconnected = true;
            printf("Client %d disconnected\n", sock_fd);
        }
    }

    void clear() {
        if(dh != nullptr) {
            dh->clear();
            delete dh;
        }
        if(connectionThread != nullptr) {
            if(connectionThread->joinable()) connectionThread->join();
        }
        if(mainThread != nullptr) {
            if(mainThread->joinable()) mainThread->join();
        }
        if(converter != nullptr) {
            converter->Clear();
            delete converter;
        }
    }
};

class Server{
private:
    std::thread *mainThread;
    int listener;
    bool allowMultiConnection;
    uint16_t port = 0;
    int error = -1;

public:
    std::vector<client*> Clients;

    Server(bool allowMultiConnection){
        this->allowMultiConnection = allowMultiConnection;
    }

    int start(uint16_t _port) {
        port = _port;
        signal(SIGPIPE, SIG_IGN);
        sockaddr_in addr{};

        listener = socket(AF_INET, SOCK_STREAM, 0);
        if(listener < 0)
        {
            perror("socket");
            error = 1;
            return 1;
        }

        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        addr.sin_addr.s_addr = htonl(INADDR_ANY);
        if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        {
            perror("bind");
            error = 2;
            return 2;
        }

        listen(listener, 1);
        mainThread = new std::thread([](Server *server){
            int sock_fd;
            do {

                sock_fd = accept(server->listener, nullptr, nullptr);
                if(sock_fd < 0){
                    errno = 0;
                    break;
                }
                else{
                    client *x;
                    x = new client(sock_fd);
                    server->Clients.push_back(x);
                    int value = 1;
                    if (setsockopt(sock_fd, IPPROTO_TCP, TCP_NODELAY, (char*)&value, sizeof(int)))
                    {
                        printf("\n Error : SetSockOpt TCP_NODELAY Failed \n");
                    }
                    printf("Client %d connected\n", sock_fd);
                    x->pairing();
                }


            } while (server->allowMultiConnection);
            close(server->listener);
        }, this);
        error = 0;
        return 0;
    }

    uint16_t getPort() { return port; }

    int getError() { return error; }

    void terminate(){
        for(auto i: Clients){ i->terminate(); }
        shutdown(listener, SHUT_RDWR);
        mainThread->join();
        close(listener);
        error = -1;
    }

    void restart(){
        terminate();
        start(port);
    }
};
#endif //STREAM_SCREEN_SERVER_H
