#ifndef STREAM_SCREEN_TRANSLATION_H
#define STREAM_SCREEN_TRANSLATION_H

#include "../Video/Outputs.h"

class Translation{
private:
    bool available;
public:
    Capture *capture;
    bool enabled;
    std::u16string name;
    bool confirmed;

    Translation(Capture *capture, char16_t *name){
        this->capture = capture;
        confirmed = true;
        available = (capture->update() == 0);

        enabled = true;
        this->name = name;
    }

    void update(){
        if(!confirmed) {
            available = false;
            return;
        }
        if(enabled || !available){
            available = (capture->update() == 0);
        }
        confirmed = false;
    }
    bool isAvailable() {return available;}

    bool checkAvailable() {
        available = (capture->update() == 0);
        return available;
    }

};

void update_translations(std::vector<XDisplay*> *displays, std::vector<Translation*> *Translations){
    for (auto & display : *displays) {
        display->updateOutputs();
        for (auto crtc: display->crtcs) {
            bool found = false;
            std::u16string name = to_u16string(display->outputs[crtc.second]->name).data();
            for (auto translation: *Translations) {
                if(translation->name == name){
                    found = true;
                    translation->confirmed = true;
                    translation->capture->x = crtc.first->x;
                    translation->capture->y = crtc.first->y;
                    translation->capture->resize(crtc.first->width, crtc.first->height);
                }
            }
            if(!found) {
                Translations->emplace_back(new Translation(
                        new Capture(
                                display,
                                display->root,
                                crtc.first->x,
                                crtc.first->y,
                                crtc.first->width,
                                crtc.first->height),
                        to_u16string(display->outputs[crtc.second]->name).data()));
                Translations->back()->confirmed = true;
            }
        }
    }
}

#endif //STREAM_SCREEN_TRANSLATION_H
