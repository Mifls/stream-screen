#ifndef UNTITLED_XWINDOW_H
#define UNTITLED_XWINDOW_H

#include "XDisplay.h"

class XWindow {
private:
    size_t width = 1024;
    size_t height = 768;
    bool visible;

public:
    XDisplay *xDisplay;
    GC gc;				/* Графический контекст */
    XEvent event;
    Window window;
    Atom wmDeleteMessage;
    XWindowAttributes attribs;
    bool created = false;
    GLXContext context;

    XWindow(XDisplay *xDisplay) {
        this->xDisplay = xDisplay;
        Init(true);
    }

    XWindow(XDisplay *xDisplay, size_t width, size_t height, bool show) {
        this->xDisplay = xDisplay;
        this->width = width;
        this->height = height;
        Init(show);
    }

    int Init(bool show){

        // Check GLX version
        GLint majorGLX, minorGLX = 0;
        glXQueryVersion(this->xDisplay->display, &majorGLX, &minorGLX);
        if (majorGLX <= 1 && minorGLX < 2) {
            printf("GLX 1.2 or greater is required.\n");
            XCloseDisplay(this->xDisplay->display);
            return 1;
        }
        else {
            printf("GLX version: %d.%d\n", majorGLX, minorGLX);
        }

        // Open the window
        XSetWindowAttributes windowAttributes;
        windowAttributes.background_pixel = 0x2f2f2f;
        windowAttributes.colormap = XCreateColormap(this->xDisplay->display, this->xDisplay->root, this->xDisplay->visualInfo->visual, AllocNone);
        windowAttributes.event_mask = ExposureMask|KeyReleaseMask|KeyPressMask|ButtonReleaseMask|ButtonPressMask;

        window = XCreateWindow(this->xDisplay->display, this->xDisplay->root, 0, 0, this->width, this->height, 0,
                               this->xDisplay->visualInfo->depth, InputOutput, this->xDisplay->visualInfo->visual,
                               CWBackPixel | CWColormap | CWEventMask, &windowAttributes);

        wmDeleteMessage = XInternAtom(this->xDisplay->display, "WM_DELETE_WINDOW", False);
        XSetWMProtocols(this->xDisplay->display, this->window, &wmDeleteMessage, 1);

        // Create GLX OpenGL context
        context = glXCreateContext(this->xDisplay->display, this->xDisplay->visualInfo, NULL, GL_TRUE);
        glXMakeCurrent(this->xDisplay->display, this->window, context);

        printf("GL Vendor: %s\n", glGetString(GL_VENDOR));
        printf("GL Renderer: %s\n", glGetString(GL_RENDERER));
        printf("GL Version: %s\n", glGetString(GL_VERSION));
        printf("GL Shading Language: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

        gc = XCreateGC(this->xDisplay->display, window, 0, nullptr);

        // Show the window
        if(show) this->show();


        updateAttribs();

        created = true;
    }

    void show() {
        XClearWindow(xDisplay->display, window);
        XMapWindow(xDisplay->display, window);
        XFlush(xDisplay->display);
        visible = true;
    }

    void hide() {
        XUnmapWindow(xDisplay->display, window);
        XFlush(xDisplay->display);
        visible = false;
    }

    bool getVisible() { return visible; }

    void updateAttribs() {
        if(created)
            XGetWindowAttributes(this->xDisplay->display, window, &attribs);
    }

    void close() {
        if(created) {
            XDestroyWindow(this->xDisplay->display, window);
            delete(this);
        }
    }
};

#endif //UNTITLED_XWINDOW_H