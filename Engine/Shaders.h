#ifndef OPENGL_LESSONS_SHADERS_H
#define OPENGL_LESSONS_SHADERS_H

#include "../Headers.h"
#include <string>
#include <fstream>
#include <sstream>

GLuint ReadShaderCode(const char * shader_file_path, std::string *ShaderCode);
GLuint CompileShader(const char * shader_file_path, const char * ShaderSourcePointer, GLuint ShaderID);
GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);

class ShaderProgram{
public:
    char * vertex_file_path;
    char * fragment_file_path;

    GLuint ProgramID;

    ShaderProgram(char * vertex_file_path, char * fragment_file_path){
        this->vertex_file_path = vertex_file_path;
        this->fragment_file_path = fragment_file_path;
    }

    GLuint loadFromFile(){
        this->ProgramID = LoadShaders(this->vertex_file_path, this->fragment_file_path);
        return 0;
    }

    void Use(){
        glUseProgram(this->ProgramID);
    }
};


GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path){

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    // Read the Vertex Shader code from the file
    std::string VertexShaderCode;
    if(ReadShaderCode(vertex_file_path, &VertexShaderCode) == -1) return 0;

    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    if(ReadShaderCode(fragment_file_path, &FragmentShaderCode) == -1) return 0;

    GLint Result = GL_FALSE;
    int InfoLogLength;

    // Compile Vertex Shader
    CompileShader(vertex_file_path, VertexShaderCode.c_str(), VertexShaderID);

    // Compile Fragment Shader
    CompileShader(fragment_file_path, FragmentShaderCode.c_str(), FragmentShaderID);

    // Link the program
    printf("Linking program\n");
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);

    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
        glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    }

    glDetachShader(ProgramID, VertexShaderID);
    glDetachShader(ProgramID, FragmentShaderID);

    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

    printf("Done.\n");
    return ProgramID;
}

GLuint ReadShaderCode(const char * shader_file_path, std::string *ShaderCode){
    std::ifstream ShaderStream(shader_file_path, std::ios::in);
    if(ShaderStream.is_open()){
        std::stringstream sstr;
        sstr << ShaderStream.rdbuf();
        *ShaderCode = sstr.str();
        ShaderStream.close();
        return 0;
    }else{
        printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", shader_file_path);
        getchar();
        return -1;
    }
}

GLuint CompileShader(const char * shader_file_path, const char * ShaderSourcePointer, GLuint ShaderID){
    GLint Result = GL_FALSE;
    int InfoLogLength;

    printf("Compiling shader : %s\n", shader_file_path);
    glShaderSource(ShaderID, 1, &ShaderSourcePointer , NULL);
    glCompileShader(ShaderID);

    // Check Vertex Shader
    glGetShaderiv(ShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(ShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> ShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(ShaderID, InfoLogLength, NULL, &ShaderErrorMessage[0]);
        printf("%s\n", &ShaderErrorMessage[0]);
        return -1;
    }
    return 0;
}

#endif //OPENGL_LESSONS_SHADERS_H