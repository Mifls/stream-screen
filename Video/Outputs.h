#ifndef STREAM_SCREEN_OUTPUTS_H
#define STREAM_SCREEN_OUTPUTS_H

#include "../Engine/XWindow.h"
#include "Capture.h"
#include "../Interface/Controls.h"
#include "../Engine/OpenGL_frame_processor.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
#include <libavformat/avformat.h>
}

class SimpleXWindowShmOutput{
    XWindow *xWindow;
    Capture *capture;

public:
    SimpleXWindowShmOutput(Capture *capture, XDisplay *xDisplay){
        this->capture = capture;

        xWindow = new XWindow(xDisplay, this->capture->image->width, this->capture->image->height, true);
        if (!xWindow->created) return;
    }

    int update(){
        while (XCheckWindowEvent(this->xWindow->xDisplay->display, this->xWindow->window, ExposureMask, &this->xWindow->event)) {

            switch ( this->xWindow->event.type ) {
                case Expose :

                    if (this->xWindow->event.xexpose.count != 0 )
                        break;

                    //Some actions

                    XFlush(this->xWindow->xDisplay->display);
                    break;

            }
        }

        XShmPutImage(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->gc, this->capture->image, 0, 0, 0, 0, this->capture->image->width, this->capture->image->height, false);
        XFlush(this->xWindow->xDisplay->display);
        return 0;
    }
};

class OpenGLXWindowOutput{
    XWindow *xWindow;
    Capture *capture;
    size_t width, height;
    controls::rectangle *img;
    InterfaceContext *interfaceContext;

public:
    OpenGLXWindowOutput(Capture *capture, XDisplay *xDisplay, size_t width, size_t height, ShaderProgram *shader){
        this->capture = capture;

        this->width = width;
        this->height = height;

        xWindow = new XWindow(xDisplay, width, height, true);
        if (!xWindow->created) return;

        interfaceContext = new InterfaceContext();
        interfaceContext->xWindow = this->xWindow;

        glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);

        if(shader == nullptr){
            interfaceContext->rectangleShader = new ShaderProgram((GLchar*)"../Resources/Shaders/ImageVertexShader.sdr",
                                                                  (GLchar*)"../Resources/Shaders/ImageFragmentShader.sdr");
            this->interfaceContext->rectangleShader->loadFromFile();
        }
        else{
            interfaceContext->rectangleShader = shader;
        }

        glXMakeCurrent(xDisplay->display, this->xWindow->window, this->xWindow->context);
        img = new controls::rectangle(interfaceContext, 0.0f, 0.0f, this->width, this->height, this->capture->image);
    }

    int update(){
        glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);

        while (XCheckWindowEvent(this->xWindow->xDisplay->display, this->xWindow->window, ExposureMask, &this->xWindow->event)) {

            switch ( this->xWindow->event.type ) {
                case Expose :

                    if (this->xWindow->event.xexpose.count != 0 )
                        break;

                    this->xWindow->updateAttribs();
                    glViewport(0, 0, this->xWindow->attribs.width, this->xWindow->attribs.height);

                    this->width = this->xWindow->attribs.width;
                    this->height = this->xWindow->attribs.height;
                    img->width = this->width;
                    img->height = this->height;

                    XFlush(this->xWindow->xDisplay->display);
                    break;

            }
        }

        glClear(GL_COLOR_BUFFER_BIT);

        img->update();
        img->redraw();

        glFlush();
        glXSwapBuffers(this->xWindow->xDisplay->display, this->xWindow->window);
        XFlush(this->xWindow->xDisplay->display);

        return 0;
    }
};

class MpegConverter{
private:
    int ret, framenum;
    AVFrame *frame;

    OpenGLFrameProcessor *processor;

public:
    XDisplay *xDisplay;
    const AVCodec *codec;
    AVCodecContext *context= nullptr;
    AVPacket *pkt;
    int encodingStatus;
    XImage *image;

    MpegConverter(XDisplay *xDisplay, XImage *image){
        this->image = image;
        this->xDisplay = xDisplay;

        /* find the mpeg1video encoder */
        //codec = avcodec_find_encoder_by_name(codec_name);
        codec = avcodec_find_encoder(AV_CODEC_ID_H264);

        context = avcodec_alloc_context3(codec);
        if (!context) {
            fprintf(stderr, "Could not allocate video codec context\n");
            return;
        }

        pkt = av_packet_alloc();
        if (!pkt)
            return;

        /* put sample parameters */
        context->bit_rate = 10000000;
        /* resolution must be a multiple of two */
        context->width = image->width;
        context->height = image->height;
        /* frames per second */
        context->time_base = (AVRational){1, 25};
        context->framerate = (AVRational){25, 1};

        /* emit one intra frame every ten frames
         * check frame pict_type before passing frame
         * to encoder, if frame->pict_type is AV_PICTURE_TYPE_I
         * then gop_size is ignored and the output of encoder
         * will always be I frame irrespective to gop_size
         */
        context->gop_size = 200;
        context->max_b_frames = 1;
        context->pix_fmt = AV_PIX_FMT_YUV444P;

        context->me_range = 24;
        context->trellis = 2;

        //context->thread_count = 1;

        framenum = 0;

        if (codec->id == AV_CODEC_ID_H264) {
            av_opt_set(context->priv_data, "preset", "ultrafast", 0);
            av_opt_set(context->priv_data, "tune", "zerolatency", 0);
            //av_opt_set(context->priv_data, "profile", "baseline", 0);
            //av_opt_set(context->priv_data, "hwaccel", "qsv", 0);
        }

        /* open it */
        ret = avcodec_open2(context, codec, nullptr);
        if (ret < 0) {
            //fprintf(stderr, "Could not open codec: %s\n", av_err2str(ret));
            return;
        }

        frame = av_frame_alloc();
        if (!frame) {
            fprintf(stderr, "Could not allocate video frame\n");
            exit(1);
        }
        frame->format = context->pix_fmt;
        frame->width  = context->width;
        frame->height = context->height;

        ret = av_frame_get_buffer(frame, 0);
        if (ret < 0) {
            fprintf(stderr, "Could not allocate the video frame data\n");
            return;
        }

        encodingStatus = -1;

        processor = new OpenGLFrameProcessor(xDisplay, context->width, context->height);
        processor->compileShader((GLchar*)"../Resources/Shaders/RGB2YCbCrVertexShader.sdr",
                                 (GLchar*)"../Resources/Shaders/RGB2YCbCrFragmentShader.sdr");
        processor->addData((GLchar*)"RGB", nullptr, GL_RGBA, GL_RGBA, 0, 0);
    }

    int update(){
        fflush(stdout);

        /* make sure the frame data is writable */
        ret = av_frame_make_writable(frame);
        if (ret < 0)
            return -1;

        /* prepare a dummy image */

        processor->setSize(image->width, image->height);

        processor->changeData(0, (GLchar*)image->data, GL_RGBA, GL_BGRA, image->width, image->height);

        processor->render();
        processor->getData((GLuint*)frame->data[0], GL_RED);
        processor->getData((GLuint*)frame->data[1], GL_GREEN);
        processor->getData((GLuint*)frame->data[2], GL_BLUE);

        frame->pts = framenum;
        framenum++;
        /* send the frame to the encoder */
        encodingStatus = avcodec_send_frame(context, frame);
        if (encodingStatus < 0) {
            fprintf(stderr, "Error sending a frame for encoding\n");
            return -1;
        }

        return 0;
    }

    bool getPacket(uint8_t **data, size_t *data_size) {
        while (encodingStatus >= 0) {
            encodingStatus = avcodec_receive_packet(context, pkt);
            if (encodingStatus == AVERROR(EAGAIN) || encodingStatus == AVERROR_EOF)
                continue;
            else if (encodingStatus < 0) {
                fprintf(stderr, "Error during encoding\n");
                return -1;
            }

            //printf("Write packet %3" PRId64 " (size=%5d)\n", pkt->pts, pkt->size);

            *data_size = pkt->size;
            *data = pkt->data;

            return true;
        }
        return false;
    }

    int getFramenum() {return framenum;}

    void wipePacket() {av_packet_unref(pkt);}

    void Clear(){
        avcodec_free_context(&context);
        av_frame_free(&frame);
        av_packet_free(&pkt);
    }
};

class MpegFileOutput{

    FILE *f;
    uint8_t *endcode;

public:
    MpegFileOutput(MpegConverter *converter, const char *filename){
        this->converter = converter;

        this->f = fopen(filename, "wb");
        if (!this->f) {
            fprintf(stderr, "Could not open %s\n", filename);
            return;
        }

        endcode = new uint8_t[4]{ 0, 0, 1, 0xb7 };
    }

    int update(){
        while (converter->encodingStatus >= 0) {
            converter->encodingStatus = avcodec_receive_packet(converter->context, converter->pkt);
            if (converter->encodingStatus == AVERROR(EAGAIN) || converter->encodingStatus == AVERROR_EOF)
                return -1;
            else if (converter->encodingStatus < 0) {
                fprintf(stderr, "Error during encoding\n");
                return -1;
            }

            printf("Write packet %3" PRId64 " (size=%5d)\n", converter->pkt->pts, converter->pkt->size);
            fwrite(converter->pkt->data, 1, converter->pkt->size, f);
            av_packet_unref(converter->pkt);
        }
        return 0;
    }

    void Clear(){
        //encode(c, NULL, pkt, f);
        converter->encodingStatus = avcodec_send_frame(converter->context, nullptr);
        if (converter->encodingStatus < 0) {
            fprintf(stderr, "Error sending a frame for encoding\n");
            exit(1);
        }

        while (converter->encodingStatus >= 0) {
            converter->encodingStatus = avcodec_receive_packet(converter->context, converter->pkt);
            if (converter->encodingStatus == AVERROR(EAGAIN) || converter->encodingStatus == AVERROR_EOF)
                return;
            else if (converter->encodingStatus < 0) {
                fprintf(stderr, "Error during encoding\n");
                exit(1);
            }

            printf("Write packet %3" PRId64 " (size=%5d)\n", converter->pkt->pts, converter->pkt->size);
            fwrite(converter->pkt->data, 1, converter->pkt->size, f);
            av_packet_unref(converter->pkt);
        }
        if (converter->codec->id == AV_CODEC_ID_MPEG1VIDEO || converter->codec->id == AV_CODEC_ID_MPEG2VIDEO)
            fwrite(endcode, 1, sizeof(endcode), f);
        fclose(f);
    }

    MpegConverter *converter;
};

class MpegWindowOutput{
#define INBUF_SIZE 4096

    const AVCodec *codec;
    AVCodecParserContext *parser;
    AVCodecContext *c= nullptr;
    AVFrame *frame;
    uint8_t inbuf[INBUF_SIZE + AV_INPUT_BUFFER_PADDING_SIZE];
    uint8_t *data;
    size_t data_size;
    int ret;
    AVPacket *pkt;

    OpenGLFrameProcessor *processor;

public:
    MpegConverter *converter;
    XImage *img;

    MpegWindowOutput(MpegConverter *converter){
        this->converter = converter;

        img = XCreateImage(converter->xDisplay->display,
                           converter->xDisplay->visualInfo->visual, 24, ZPixmap, 0,
                           nullptr, 1920, 1080, 32, 0);
        img->data = (char *) (new int[1920*1080]);


        processor = new OpenGLFrameProcessor(converter->xDisplay, 1920, 1080);
        processor->compileShader((GLchar*)"../Resources/Shaders/YCbCr2RGBVertexShader.sdr",
                                 (GLchar*)"../Resources/Shaders/YCbCr2RGBFragmentShader.sdr");

        processor->addData((GLchar*)"Y", nullptr, GL_RED, GL_RED, 0, 0);
        processor->addData((GLchar*)"Cb", nullptr, GL_RED, GL_RED, 0, 0);
        processor->addData((GLchar*)"Cr", nullptr, GL_RED, GL_RED, 0, 0);

        pkt = av_packet_alloc();
        if (!pkt)
            exit(1);
        /* set end of buffer to 0 (this ensures that no overreading happens for damaged MPEG streams) */
        memset(inbuf + INBUF_SIZE, 0, AV_INPUT_BUFFER_PADDING_SIZE);
        /* find the MPEG-1 video decoder */
        codec = avcodec_find_decoder(AV_CODEC_ID_H264);
        if (!codec) {
            fprintf(stderr, "Codec not found\n");
            exit(1);
        }
        parser = av_parser_init(codec->id);
        if (!parser) {
            fprintf(stderr, "parser not found\n");
            exit(1);
        }
        c = avcodec_alloc_context3(codec);
        if (!c) {
            fprintf(stderr, "Could not allocate video codec context\n");
            exit(1);
        }

        //c->thread_count = 12;
        c->flags |= AVFMT_FLAG_NOBUFFER | AVFMT_FLAG_FLUSH_PACKETS;
        c->flags |= AV_CODEC_FLAG_LOW_DELAY;

        if (codec->id == AV_CODEC_ID_H264) {
            //av_opt_set( c->priv_data, "probesize", "32", 0 );
            //av_opt_set( c->priv_data, "analyzeduration", "0", 0 );
            av_opt_set( c->priv_data, "avioflags", "direct", 0 );
        }

        /* For some codecs, such as msmpeg4 and mpeg4, width and height
           MUST be initialized there because this information is not
           available in the bitstream. */
        /* open it */
        if (avcodec_open2(c, codec, nullptr) < 0) {
            fprintf(stderr, "Could not open codec\n");
            exit(1);
        }

        frame = av_frame_alloc();
        if (!frame) {
            fprintf(stderr, "Could not allocate video frame\n");
            exit(1);
        }
    }

    int update(){
        while (converter->getPacket(&data, &data_size)) {

            if (!data_size)
                break;

            while (data_size > 0) {
                ret = av_parser_parse2(parser, c, &pkt->data, &pkt->size,
                                       data, data_size, AV_NOPTS_VALUE, AV_NOPTS_VALUE, 0);
                if (ret < 0) {
                    fprintf(stderr, "Error while parsing\n");
                    exit(1);
                }
                data      += ret;
                data_size -= ret;
                if (pkt->size){
                    ret = avcodec_send_packet(c, pkt);
                    if (ret < 0) {
                        fprintf(stderr, "Error sending a packet for decoding\n");
                        exit(1);
                    }
                    while (ret >= 0) {
                        ret = avcodec_receive_frame(c, frame);
                        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
                            break;
                        else if (ret < 0) {
                            fprintf(stderr, "Error during decoding\n");
                            exit(1);
                        }
                        printf("saving frame %3d\n", converter->getFramenum() - c->frame_number);
                        fflush(stdout);
                        /* the picture is allocated by the decoder. no need to
                           free it */

                        uint32_t l = frame->height*frame->width;


                        if(img != nullptr){
                            if((img->width != frame->width)||(img->height != frame->height)){
                                XDestroyImage(img);
                                img = XCreateImage(converter->xDisplay->display,
                                                   converter->xDisplay->visualInfo->visual, 24, ZPixmap, 0,
                                                   nullptr, frame->width, frame->height, 32, 0);
                                img->data = (char *) (new int[l]);
                                processor->setSize(frame->width, frame->height);
                            }
                        }
                        else{
                            img = XCreateImage(converter->xDisplay->display,
                                               converter->xDisplay->visualInfo->visual, 24, ZPixmap, 0,
                                               nullptr, frame->width, frame->height, 32, 0);
                            img->data = (char *) (new int[l]);
                            processor->setSize(frame->width, frame->height);
                        }

                        processor->changeData(0, (GLchar*)frame->data[0], GL_RED, GL_RED, frame->width, frame->height);
                        processor->changeData(1, (GLchar*)frame->data[1], GL_RED, GL_RED, frame->width, frame->height);
                        processor->changeData(2, (GLchar*)frame->data[2], GL_RED, GL_RED, frame->width, frame->height);

                        processor->render();
                        processor->getData((GLuint*)img->data, GL_RGBA);

                    }
                }
            }
            converter->wipePacket();
        }
        return 0;
    }

    void Clear(){

    }
};

#endif //STREAM_SCREEN_OUTPUTS_H
