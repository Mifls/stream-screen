#ifndef UNTITLED_COMPOSE_H
#define UNTITLED_COMPOSE_H

#define GoldenRatio 1.618034f

#define extract_A(pixel) ((uint8_t)((pixel >> 24) & 0xff))
#define extract_R(pixel) ((uint8_t)((pixel >> 16) & 0xff))
#define extract_G(pixel) ((uint8_t)((pixel >> 8) & 0xff))
#define extract_B(pixel) ((uint8_t)((pixel >> 0) & 0xff))
#define create_pixel(a, r, g, b) \
((((size_t)a)<<24) + (((size_t)r)<<16) + (((size_t)g)<<8) + b)

#define RGB_to_Y(r, g, b) ((0.257 * ((uint8_t)r)) + (0.504 * ((uint8_t)g)) + (0.098 * ((uint8_t)b)) + 16)
#define RGB_to_Cb(r, g, b) ((-0.148 * ((uint8_t)r)) + (-0.291 * ((uint8_t)g)) + (0.439 * ((uint8_t)b)) + 128)
#define RGB_to_Cr(r, g, b) ((0.439 * ((uint8_t)r)) + (-0.368 * ((uint8_t)g)) + (-0.071 * ((uint8_t)b)) + 128)

int MergePixels(unsigned int background, unsigned int pixel){
    uint8_t r,g,b,a;
    uint8_t backR, backG,backB;

    a = extract_A(pixel);

    backR = extract_R(background);
    r = extract_R(pixel);
    backG = extract_G(background);
    g = extract_G(pixel);
    backB = extract_B(background);
    b = extract_B(pixel);

    r = (uint8_t)(((size_t)backR * (255-a) + (size_t)r * a)/255);
    g = (uint8_t)(((size_t)backG * (255-a) + (size_t)g * a)/255);
    b = (uint8_t)(((size_t)backB * (255-a) + (size_t)b * a)/255);

    return create_pixel(a, r, g, b);
}

void InsertPixels(XImage* background, size_t* pixels, size_t width, size_t height, int x, int y){

    int row,col;
    size_t pos;
    size_t minRow,minCol;

    minCol = std::max(x, 0);
    minRow = std::max(y, 0);

    int maxY = std::min((int)height + y, background->height);
    int maxX = std::min((int)width + x, background->width);

    size_t dBegin = std::max((int)minCol - x, 0);
    size_t dEnd = width + x - maxX;

    size_t pixel;

    pos = (abs(y) - minRow) * width;

    for(row = minRow;row < maxY; row++)
    {
        pos += dBegin;
        for(col=minCol;col < maxX;col++,pos++)
        {
            pixel = ((unsigned int*)(background->data))[row * background->width + col];
            pixel = MergePixels(pixel, pixels[pos]);

            XPutPixel(background, col, row, pixel);
        }
        pos += dEnd;
    }
}

float ScaleArea(float src_x, float src_y, float sc_x, float sc_y){
    float tmp = sc_x/sc_y*src_y;
    if(tmp>src_x){
        return src_x/sc_x;
    }
    else{
        return src_y/sc_y;
    }
}

bool PointInArea(float area_x, float area_y, float area_width, float area_height, float x, float y){
    return((x >= area_x)&&(y >= area_y)&&(x <= area_x + area_width)&&(y <= area_y + area_height));
}

#endif //UNTITLED_COMPOSE_H