#ifndef STREAM_SCREEN_CAPTURE_H
#define STREAM_SCREEN_CAPTURE_H

#include <X11/extensions/Xfixes.h>
#include <X11/extensions/XShm.h>
#include <zconf.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include "Compose.h"
#include "../Headers.h"
#include "../Engine/XDisplay.h"

class Capture{
private:

public:
    XDisplay *xDisplay;
    Window src;
    XImage *image = nullptr;
    //XImage *subimage = nullptr;
    XFixesCursorImage *img = nullptr;
    XShmSegmentInfo *shm_info;

    size_t x, y;

    Capture(XDisplay *xDisplay, Window src, size_t x, size_t y, size_t width, size_t height){
        this->xDisplay = xDisplay;
        this->src = src;

        this->x = x;
        this->y = y;

        shm_info = new XShmSegmentInfo();
        image = XShmCreateImage(this->xDisplay->display,
                                this->xDisplay->visualInfo->visual,
                                this->xDisplay->visualInfo->depth,
                                ZPixmap, nullptr, shm_info,
                                width, height);

        shm_info->shmid = shmget(IPC_PRIVATE, image->bytes_per_line * image->height, IPC_CREAT | 0777);
        image->data = (char*)shmat(shm_info->shmid, nullptr, 0);
        shm_info->shmaddr = image->data;
        shm_info->readOnly = False;
        XShmAttach(this->xDisplay->display, shm_info);
        XSync(xDisplay->display, False);
        shmctl(shm_info->shmid, IPC_RMID, nullptr);
    }

    int update(){
        if((xDisplay->rootAttribs.width >= x + image->width) && (xDisplay->rootAttribs.height >= y + image->height)) {
            XSync(xDisplay->display, False);
            XShmGetImage(xDisplay->display, src, image, x, y, AllPlanes);
            img = XFixesGetCursorImage(xDisplay->display);
            XSync(xDisplay->display, False);
        }
        else return -1;
        //image = XGetImage(display, root, 0, 0, 2944, 1080, XAllPlanes(), ZPixmap);

        InsertPixels(image, img->pixels, img->width, img->height, img->x - img->xhot - x, img->y-img->yhot-y);
        delete [] img;

        //if(image->data != nullptr)XDestroyImage(image);
        //if(subimage != nullptr)XDestroyImage(subimage);
        return 0;
    }

    bool resize(size_t width, size_t height){
        bool ret = (image->width == width) && (image->height == height);
        if(ret) return false;
        shmdt(shm_info->shmaddr);
        XDestroyImage(image);

        image = XShmCreateImage(this->xDisplay->display,
                                this->xDisplay->visualInfo->visual,
                                this->xDisplay->visualInfo->depth,
                                ZPixmap, nullptr, shm_info,
                                width, height);

        shm_info->shmid = shmget(IPC_PRIVATE, image->bytes_per_line * image->height, IPC_CREAT | 0777);
        image->data = (char*)shmat(shm_info->shmid, nullptr, 0);
        shm_info->shmaddr = image->data;
        shm_info->readOnly = False;
        XShmAttach(this->xDisplay->display, shm_info);
        XSync(xDisplay->display, False);
        shmctl(shm_info->shmid, IPC_RMID, nullptr);
        return true;
    }

};

#endif //STREAM_SCREEN_CAPTURE_H