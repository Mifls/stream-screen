#ifndef STREAM_SCREEN_STRINGS_H
#define STREAM_SCREEN_STRINGS_H

#include <cstring>
#include <locale>
#include <codecvt>

std::u16string to_u16string(int const &i) {
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t, 0x10ffff, std::little_endian>, char16_t> conv;
    return conv.from_bytes(std::to_string(i));
}

std::u16string to_u16string(char *src) {
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>,char16_t> conv;
    return conv.from_bytes(src);
}

std::string to_string(std::u16string src) {
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>,char16_t> conv;
    return conv.to_bytes(src.c_str());
}

class Strings{
public:
    static std::u16string Screens;
    static std::u16string Devices;
    static std::u16string Cancel;
    static std::u16string Ok;
    static std::u16string Frequency;
    static std::u16string Position;
    static std::u16string Size;
    static std::u16string NewScreen;
    static std::u16string Add;

    static void init() {
        Screens = u"Screens";
        Devices = u"Devices";
        Cancel = u"Cancel";
        Ok= u"Ok";
        Frequency = u"Frequency";
        Position = u"Position";
        Add = u"Add";
        NewScreen = u"New screen";
        Size = u"Size";
    }

    Strings(char* locale){

        if(strcmp(locale, "ru_RU.UTF-8") == 0){
            Screens = u"Экраны";
            Devices = u"Устройства";
            return;
        }
    }
};

std::u16string Strings::Screens;
std::u16string Strings::Devices;
std::u16string Strings::Cancel;
std::u16string Strings::Ok;
std::u16string Strings::Frequency;
std::u16string Strings::Position;
std::u16string Strings::Size;
std::u16string Strings::NewScreen;
std::u16string Strings::Add;

#endif //STREAM_SCREEN_STRINGS_H
