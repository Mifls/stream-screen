#ifndef STREAM_SCREEN_INTERFACE_H
#define STREAM_SCREEN_INTERFACE_H

#include <locale>
#include <X11/XKBlib.h>
#include <codecvt>
#include <stack>
#include <utility>
#include "../Engine/XWindow.h"
#include "../Engine/Text.h"
#include "Controls.h"
#include "Strings.h"
#include "../Engine/Translation.h"
#include "../Engine/Utils.h"
#include "../Engine/Server.h"

//int initSplash(){
//
//}

class ListPreview{
public:
    struct preview{
        XImage *image;
        std::u16string header;
        int id;
        preview(XImage *image, std::u16string header, int id){
            this->image = image;
            this->header = std::move(header);
            this->id = id;
        };
    };

private:
    InterfaceContext *interfaceContext;
    controls::text *headerText;
    controls::rectangle *headerLine;
    controls::rectangle *underLine;
    controls::screenPreview *screenPreview;
    std::vector<preview> *previews;

    GLfloat headerText_top = 13.0f/15;
    GLfloat headerText_left = 1.0f/15;
    GLfloat list_left = 1.0f/15;

    uint32_t currentPage;
    uint32_t previewsInLine;
    uint32_t linesOnPage;

public:
    uint32_t optimal_onPage = 6;

    ListPreview(InterfaceContext *interfaceContext, std::vector<preview> *previews, char16_t *header) {

        previewsInLine = 3;
        linesOnPage = 2;
        currentPage = 0;

        this->interfaceContext = interfaceContext;
        this->previews = previews;

        screenPreview = new controls::screenPreview(nullptr, interfaceContext,
                                                (uint32_t)(((float)interfaceContext->xWindow->attribs.height)*2/7*1.2f),
                                                (uint32_t)(((float)interfaceContext->xWindow->attribs.height)*2/7), 0, 0,
                                                (char16_t *)u"null");

        headerText = new controls::text(interfaceContext,
                                        (float)interfaceContext->xWindow->attribs.width*headerText_left,
                                        (float)interfaceContext->xWindow->attribs.height*headerText_top,
                                        header,
                                        glm::vec3(0.7f, 0.7f, 0.7f),
                                        interfaceContext->xWindow->attribs.height / 15, 1.f);

        headerLine = new controls::rectangle(interfaceContext,
                                             (float)interfaceContext->xWindow->attribs.width / 20,
                                             (float)interfaceContext->xWindow->attribs.height*5/6,
                                             (float)interfaceContext->xWindow->attribs.width * 0.9f, 2,
                                             glm::vec4(0.3f, 0.3f, 0.3f, 1.0f));

        underLine = new controls::rectangle(interfaceContext,
                                             (float)interfaceContext->xWindow->attribs.width / 20,
                                             (float)interfaceContext->xWindow->attribs.height / 15,
                                             (float)interfaceContext->xWindow->attribs.width * 0.9f, 2,
                                             glm::vec4(0.3f, 0.3f, 0.3f, 1.0f));
    }

    void setHeader(char16_t *_header) {
        headerText->caption = _header;
    }

    void update(){
        headerText->x = (float)interfaceContext->xWindow->attribs.width*headerText_left;
        headerText->y = (float)interfaceContext->xWindow->attribs.height*headerText_top;
        headerText->scale = ((float)interfaceContext->xWindow->attribs.height / 15) / headerText->getSize();
        headerText->update();

        headerLine->x = (float)interfaceContext->xWindow->attribs.width / 20;
        headerLine->y = (float)interfaceContext->xWindow->attribs.height * 5.f / 6;
        headerLine->width = (float)interfaceContext->xWindow->attribs.width * 0.9f;
        headerLine->update();

        underLine->x = (float)interfaceContext->xWindow->attribs.width / 20;
        underLine->y = (float)interfaceContext->xWindow->attribs.height * (2.f/7 - 1.f/6);
        underLine->width = (float)interfaceContext->xWindow->attribs.width * 0.9f;
        underLine->update();
    }

    int redraw(){
        int ret = -1;
        auto *attribs = &(interfaceContext->xWindow->attribs);

        float indent_X = (float)attribs->width / 50;
        float view_width = ((float)attribs->width * (1.f - list_left - list_left) - indent_X * (float)(previewsInLine - 1)) / (float)previewsInLine;
        float view_height = (float)(attribs->height) / (1.75f * (float)linesOnPage);

        while(view_width < view_height*(GoldenRatio - 0.1f)){
            if((optimal_onPage < previewsInLine*linesOnPage) && (previewsInLine > 1))previewsInLine--;
            else linesOnPage++;
            view_width = ((float)attribs->width * (1.f - list_left - list_left) - indent_X * (float)(previewsInLine - 1)) / (float)previewsInLine;
            view_height = (float)(attribs->height) / (1.75f * (float)linesOnPage);
        }

        while(view_width > view_height*(GoldenRatio + 0.1f)){
            if((optimal_onPage < previewsInLine*linesOnPage) && (linesOnPage > 1))linesOnPage--;
            else previewsInLine++;
            view_width = ((float)attribs->width * (1.f - list_left - list_left) - indent_X * (float)(previewsInLine - 1)) / (float)previewsInLine;
            view_height = (float)(attribs->height) / (1.75f * (float)linesOnPage);
        }

        uint32_t onPage = linesOnPage * previewsInLine;
        uint32_t first = currentPage * onPage;

        float indent_Y = (1.f - 6.f/7.f) / (float)(linesOnPage + 1) * (float)attribs->height;

        for (uint32_t i = first, j = 0; (j < onPage)&&(i < previews->size()); ++i, ++j) {

            screenPreview->width = (uint32_t)view_width;
            screenPreview->x = (uint32_t)((float)attribs->width*list_left +
                                             (view_width + indent_X) * (float)(j % previewsInLine));
            screenPreview->height = (uint32_t)view_height;
            screenPreview->y = (uint32_t)((view_height + indent_Y) * (float)(linesOnPage - (int)(j / previewsInLine) - 1) + indent_Y + (float)attribs->height*(2.f/7.f - 1.f/6.f));

            screenPreview->image = (*previews)[i].image;
            screenPreview->setText((*previews)[i].header.data());
            if(screenPreview->update()) ret = (*previews)[i].id;
            screenPreview->redraw();
        }

        headerText->redraw();
        headerLine->redraw();
        underLine->redraw();
        return ret;
    }

    void clear() {
        headerText->clear();
        delete headerText;
        headerLine->clear();
        delete headerLine;
        underLine->clear();
        delete underLine;
        screenPreview->clear();
        delete screenPreview;
    }
};

class NewDisplay{
private:
    InterfaceContext *interfaceContext;
    XWindow *xWindow;

    //FPS
    FPS_counter *fpsCounter;
    Timer *timer;
    controls::text *fpsText;
    std::u16string fpsString;

    GLfloat headerText_top = 8.3f/10;
    GLfloat headerText_left = 1.0f/15;

    controls::rectangle *headerLine;

    controls::text *headerText;

    std::u16string xCaption = u"x";

    controls::text *sizeText;
    controls::textBox *widthTB;
    controls::textBox *heightTB;
    controls::text *sizeXText;

    controls::text *posText;
    controls::textBox *posXTB;
    controls::textBox *posYTB;
    controls::text *posXText;

    controls::text *freqText;
    controls::textBox *freqTB;

    controls::button *okButton;

    controls::button *cancelButton;

public:

    explicit NewDisplay(XWindow *window) {
        this->xWindow = window;
        this->xWindow->updateAttribs();

        glEnable(GL_TEXTURE_2D);
        glShadeModel(GL_SMOOTH);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glClearColor((float)0x2f/255, (float)0x2f/255, (float)0x2f/255, 1.0f);

        this->interfaceContext = new InterfaceContext();
        interfaceContext->xWindow = window;

        glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);
        this->interfaceContext->rectangleShader = new ShaderProgram((char*)"../Resources/Shaders/ImageVertexShader.sdr",
                                                                    (char*)"../Resources/Shaders/ImageFragmentShader.sdr");
        this->interfaceContext->rectangleShader->loadFromFile();

        this->interfaceContext->textShader = new ShaderProgram((char*)"../Resources/Shaders/TextVertexShader.sdr",
                                                               (char*)"../Resources/Shaders/TextFragmentShader.sdr");
        this->interfaceContext->textShader->loadFromFile();


        timer = new Timer();
        fpsCounter = new FPS_counter;

        headerText = new controls::text(interfaceContext,
                                        (float)interfaceContext->xWindow->attribs.width*headerText_left,
                                        (float)interfaceContext->xWindow->attribs.height*headerText_top,
                                        Strings::NewScreen.data(),
                                        glm::vec3(0.7f, 0.7f, 0.7f),
                                        interfaceContext->xWindow->attribs.height / 10, 1.f);


        headerLine = new controls::rectangle(interfaceContext,
                                             (float)interfaceContext->xWindow->attribs.width / 20,
                                             (float)interfaceContext->xWindow->attribs.height*5/6,
                                             (float)interfaceContext->xWindow->attribs.width * 0.9f, 2,
                                             glm::vec4(0.3f, 0.3f, 0.3f, 1.0f));

        sizeText = new controls::text(interfaceContext,
                                      (float)interfaceContext->xWindow->attribs.width * headerText_left,
                                      (float)interfaceContext->xWindow->attribs.height* 0.65f,
                                      Strings::Size.data(),
                                      glm::vec3(0.7f, 0.7f, 0.7f),
                                      interfaceContext->xWindow->attribs.height / 12, 1.f);

        widthTB = new controls::textBox(interfaceContext,
                                        (int)((float)interfaceContext->xWindow->attribs.width * 0.2f),
                                        interfaceContext->xWindow->attribs.height / 10,
                                        (uint32_t)((float)interfaceContext->xWindow->attribs.width * 0.3f),
                                        (uint32_t)((float)interfaceContext->xWindow->attribs.height* 0.63f));
        widthTB->max_len = 4;

        heightTB = new controls::textBox(interfaceContext,
                                         (int)((float)interfaceContext->xWindow->attribs.width * 0.2f),
                                         interfaceContext->xWindow->attribs.height / 10,
                                         (uint32_t)((float)interfaceContext->xWindow->attribs.width * 0.55f),
                                         (uint32_t)((float)interfaceContext->xWindow->attribs.height* 0.63f));
        heightTB->max_len = 4;

        sizeXText = new controls::text(interfaceContext,
                                       (float)interfaceContext->xWindow->attribs.width * 0.51f,
                                       (float)interfaceContext->xWindow->attribs.height * 0.65f,
                                       xCaption.data(),
                                       glm::vec3(0.7f, 0.7f, 0.7f),
                                       interfaceContext->xWindow->attribs.height / 12, 1.f);

        posText = new controls::text(interfaceContext,
                                     (float)interfaceContext->xWindow->attribs.width * headerText_left,
                                     (float)interfaceContext->xWindow->attribs.height * 0.45f,
                                     Strings::Position.data(),
                                     glm::vec3(0.7f, 0.7f, 0.7f),
                                     interfaceContext->xWindow->attribs.height / 12, 1.f);

        posXTB = new controls::textBox(interfaceContext,
                                       (int)((float)interfaceContext->xWindow->attribs.width * 0.2f),
                                       interfaceContext->xWindow->attribs.height / 10,
                                       (uint32_t)((float)interfaceContext->xWindow->attribs.width * 0.3f),
                                       (uint32_t)((float)interfaceContext->xWindow->attribs.height* 0.43f));

        posYTB = new controls::textBox(interfaceContext,
                                       (int)((float)interfaceContext->xWindow->attribs.width * 0.2f),
                                       interfaceContext->xWindow->attribs.height / 10,
                                       (uint32_t)((float)interfaceContext->xWindow->attribs.width * 0.55f),
                                       (uint32_t)((float)interfaceContext->xWindow->attribs.height* 0.43f));

        posXText = new controls::text(interfaceContext,
                                      (float)interfaceContext->xWindow->attribs.width * 0.51f,
                                      (float)interfaceContext->xWindow->attribs.height * 0.45f,
                                      xCaption.data(),
                                      glm::vec3(0.7f, 0.7f, 0.7f),
                                      interfaceContext->xWindow->attribs.height / 12, 1.f);

        freqText = new controls::text(interfaceContext,
                                      (float)interfaceContext->xWindow->attribs.width * headerText_left,
                                      (float)interfaceContext->xWindow->attribs.height * 0.25f,
                                      Strings::Frequency.data(),
                                      glm::vec3(0.7f, 0.7f, 0.7f),
                                      interfaceContext->xWindow->attribs.height / 12, 1.f);

        freqTB = new controls::textBox(interfaceContext,
                                       (int)((float)interfaceContext->xWindow->attribs.width * 0.2f),
                                       interfaceContext->xWindow->attribs.height / 10,
                                       (uint32_t)((float)interfaceContext->xWindow->attribs.width * 0.35f),
                                       (uint32_t)((float)interfaceContext->xWindow->attribs.height* 0.23f));
        freqTB->max_len = 2;

        okButton = new controls::button(interfaceContext,
                                        (int)((float)interfaceContext->xWindow->attribs.width * 0.25f),
                                        interfaceContext->xWindow->attribs.height / 9,
                                        (uint32_t)((float)interfaceContext->xWindow->attribs.width * 0.44f),
                                        (uint32_t)((float)interfaceContext->xWindow->attribs.height* 0.04f),
                                        Strings::Ok.data());

        cancelButton = new controls::button(interfaceContext,
                                            (int)((float)interfaceContext->xWindow->attribs.width * 0.25f),
                                            interfaceContext->xWindow->attribs.height / 9,
                                            (uint32_t)((float)interfaceContext->xWindow->attribs.width * 0.72f),
                                            (uint32_t)((float)interfaceContext->xWindow->attribs.height* 0.04f),
                                            Strings::Cancel.data());

        fpsText = new controls::text(interfaceContext,
                                     floor(xWindow->attribs.width*0.01),
                                     floor(xWindow->attribs.width*0.01),
                                     const_cast<char16_t *>(u"fps: "),
                                     glm::vec3(0.7f, 0.7f, 0.7f),
                                     xWindow->attribs.height / 15, 1.f);

        timer->Reset();

        update();
    }

    bool update(){
        interfaceContext->click.clicked = false;
        interfaceContext->keyPressed.pressed = false;
        while (XCheckWindowEvent(xWindow->xDisplay->display, xWindow->window, xWindow->attribs.your_event_mask, &xWindow->event)) {

            switch ( xWindow->event.type ) {
                case Expose : {

                    if (xWindow->event.xexpose.count != 0)
                        break;

                    this->xWindow->updateAttribs();
                    glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);
                    glViewport(0, 0, this->xWindow->attribs.width, this->xWindow->attribs.height);
                    fpsText->update();

                    XFlush(xWindow->xDisplay->display);
                    break;
                }
                case ButtonPress: {
                    if (this->xWindow->event.xkey.keycode == 1) {
                        interfaceContext->click.pressed = true;
                        interfaceContext->click.x = this->xWindow->event.xkey.x;
                        interfaceContext->click.y = this->xWindow->event.xkey.y;
                    }
                    break;
                }
                case ButtonRelease: {
                    if (this->xWindow->event.xkey.keycode == 1) {
                        interfaceContext->click.clicked = true;
                        interfaceContext->click.pressed = false;
                        interfaceContext->click.x = this->xWindow->event.xkey.x;
                        interfaceContext->click.y = this->xWindow->event.xkey.y;
                    }
                    break;
                }
                case KeyPress: {
                    uint32_t x = XLookupKeysym(&xWindow->event.xkey, 0);
                    interfaceContext->keyPressed.pressed = true;
                    interfaceContext->keyPressed.code = xWindow->event.xkey.keycode;
                    interfaceContext->keyPressed.symbol = (char)x;
                    break;
                }

            }


        }

        uint32_t tmp;
        Window w;
        XQueryPointer(xWindow->xDisplay->display,
                      xWindow->window, (Window*)(&tmp),
                      (Window*)(&tmp), (int*)(&tmp), (int*)(&tmp),
                      &interfaceContext->pointerPosition.x,
                      &interfaceContext->pointerPosition.y, &tmp);

        XGetInputFocus (xWindow->xDisplay->display, &w , (int*)(&tmp));
        interfaceContext->pointerOnWindow = xWindow->window == w;

        this->xWindow->xDisplay->updateScreen();

        // Header update
        headerText->x = (float)((uint32_t)((float)interfaceContext->xWindow->attribs.width * headerText_left));
        headerText->y = (float)((uint32_t)((float)interfaceContext->xWindow->attribs.height * headerText_top));
        headerText->scale = ((float)interfaceContext->xWindow->attribs.height / 10) / headerText->getSize();
        headerText->update();

        headerLine->x = (float)interfaceContext->xWindow->attribs.width / 20;
        headerLine->y = (float)interfaceContext->xWindow->attribs.height * 7.f / 9;
        headerLine->width = (float)interfaceContext->xWindow->attribs.width * 0.9f;
        headerLine->update();

        // Size fields update
        sizeText->x = (float)((uint32_t)((float)interfaceContext->xWindow->attribs.width * headerText_left));
        sizeText->y = (float)((uint32_t)((float)interfaceContext->xWindow->attribs.height * 0.65f));
        sizeText->scale = ((float)interfaceContext->xWindow->attribs.height / 12) / sizeText->getSize();
        sizeText->update();

        widthTB->x = (uint32_t)((float)interfaceContext->xWindow->attribs.width * 0.3f);
        widthTB->y = (uint32_t)((float)interfaceContext->xWindow->attribs.height* 0.63f);
        widthTB->width = (int)((float)interfaceContext->xWindow->attribs.width * 0.2f);
        widthTB->height = interfaceContext->xWindow->attribs.height / 10;
        widthTB->update();

        heightTB->x = (uint32_t)((float)interfaceContext->xWindow->attribs.width * 0.55f);
        heightTB->y = (uint32_t)((float)interfaceContext->xWindow->attribs.height* 0.63f);
        heightTB->width = (int)((float)interfaceContext->xWindow->attribs.width * 0.2f);
        heightTB->height = interfaceContext->xWindow->attribs.height / 10;
        heightTB->update();

        // Position fields update
        posText->x = (float)((uint32_t)((float)interfaceContext->xWindow->attribs.width * headerText_left));
        posText->y = (float)((uint32_t)((float)interfaceContext->xWindow->attribs.height * 0.45f));
        posText->scale = ((float)interfaceContext->xWindow->attribs.height / 12) / sizeText->getSize();
        posText->update();

        posXTB->x = (uint32_t)((float)interfaceContext->xWindow->attribs.width * 0.3f);
        posXTB->y = (uint32_t)((float)interfaceContext->xWindow->attribs.height* 0.43f);
        posXTB->width = (int)((float)interfaceContext->xWindow->attribs.width * 0.2f);
        posXTB->height = interfaceContext->xWindow->attribs.height / 10;
        posXTB->update();

        posYTB->x = (uint32_t)((float)interfaceContext->xWindow->attribs.width * 0.55f);
        posYTB->y = (uint32_t)((float)interfaceContext->xWindow->attribs.height* 0.43f);
        posYTB->width = (int)((float)interfaceContext->xWindow->attribs.width * 0.2f);
        posYTB->height = interfaceContext->xWindow->attribs.height / 10;
        posYTB->update();

        // Frequency fields update
        freqText->x = (float)((uint32_t)((float)interfaceContext->xWindow->attribs.width * headerText_left));
        freqText->y = (float)((uint32_t)((float)interfaceContext->xWindow->attribs.height * 0.25f));
        freqText->scale = ((float)interfaceContext->xWindow->attribs.height / 12) / sizeText->getSize();
        freqText->update();

        freqTB->x = (uint32_t)((float)interfaceContext->xWindow->attribs.width * 0.35f);
        freqTB->y = (uint32_t)((float)interfaceContext->xWindow->attribs.height* 0.23f);
        freqTB->width = (int)((float)interfaceContext->xWindow->attribs.width * 0.2f);
        freqTB->height = interfaceContext->xWindow->attribs.height / 10;
        freqTB->update();

        if(okButton->update()) {
            if((!to_string(widthTB->val_str).empty()) && (!to_string(heightTB->val_str).empty())
            && (!to_string(freqTB->val_str).empty())) {
                int width = std::stoi(to_string(widthTB->val_str));
                bool width_condition = (width > 0) && (width % 2 == 0);
                int height = std::stoi(to_string(heightTB->val_str));
                bool height_condition = (height > 0) && (height % 2 == 0);
                int freq = std::stoi(to_string(freqTB->val_str));
                bool freq_condition = (freq > 5) && (freq < 100);

                if (width_condition && height_condition && freq_condition) {
                    std::string name = "virtual_" + std::to_string(width) + "x" + std::to_string(height) +
                                       "_" + std::to_string(freq);

                    XRRScreenResources *screen;
                    screen = XRRGetScreenResources(xWindow->xDisplay->display, xWindow->xDisplay->root);
                    RRMode mode;
                    bool found = false;
                    for (int i = 0; i < screen->nmode; ++i) {
                        if (strcmp(screen->modes[i].name, name.data()) == 0) {
                            found = true;
                            mode = screen->modes[i].id;
                            break;
                        }
                    }

                    if (!found) {
                        auto info = XRRAllocModeInfo(name.c_str(), (int)name.size());
                        info->width = width;
                        info->hTotal = width;
                        info->hSyncStart = width;
                        info->hSyncEnd = width;
                        info->hTotal = width;

                        info->height = height;
                        info->vTotal = height;
                        info->vSyncStart = height;
                        info->vSyncEnd = height;
                        info->vTotal = height;
                        info->dotClock = freq * width * height;

                        mode = XRRCreateMode(xWindow->xDisplay->display, xWindow->xDisplay->root, info);
                    }
                    XRRAddOutputMode(xWindow->xDisplay->display, xWindow->xDisplay->freeOutputs[0], mode);

                    if ((!to_string(posXTB->val_str).empty()) && (!to_string(posYTB->val_str).empty())) {
                        int x = std::stoi(to_string(posXTB->val_str));
                        int y = std::stoi(to_string(posYTB->val_str));

                        auto screen_res = XRRGetScreenResources(xWindow->xDisplay->display, xWindow->xDisplay->root);
                        auto out_info = XRRGetOutputInfo(xWindow->xDisplay->display, screen_res, xWindow->xDisplay->freeOutputs[0]);

                        XRRSetCrtcConfig(xWindow->xDisplay->display,
                                         screen_res,
                                         out_info->crtcs[0],
                                         CurrentTime,
                                         x, y,
                                         mode,
                                         RR_Rotate_0,
                                         &xWindow->xDisplay->freeOutputs[0],
                                         1);

                    }
                }
                return true;
            }

        }
        if(cancelButton->update()) return true;
        return false;
    }

    void redraw(){
        xWindow->updateAttribs();
        glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);
        glClear(GL_COLOR_BUFFER_BIT);

        headerText->redraw();
        headerLine->redraw();

        sizeText->redraw();
        widthTB->redraw();
        heightTB->redraw();
        sizeXText->redraw();

        posText->redraw();
        posXTB->redraw();
        posYTB->redraw();
        posXText->redraw();

        freqText->redraw();
        freqTB->redraw();

        okButton->redraw();
        cancelButton->redraw();

        fpsString = u"fps: " + (std::u16string)to_u16string(fpsCounter->currentFPS);
        fpsText->caption = fpsString.data();
        fpsText->redraw();


        glFlush();
        glXSwapBuffers(xWindow->xDisplay->display, xWindow->window);
        XFlush(xWindow->xDisplay->display);
        fpsCounter->cutoff(timer->countDiff());
    }

    void show() { if( !xWindow->getVisible() ) xWindow->show(); }

    void hide() { if( xWindow->getVisible() ) xWindow->hide(); }

};

class DisplayChoice{
private:
    InterfaceContext *interfaceContext;
    XWindow *xWindow;
    std::vector<Translation*> *Translations;
    std::vector<ListPreview::preview> previews;
    ListPreview *DisplayList;
    Server *server;
    controls::text *serverText;
    std::u16string serverString;

    //FPS
    FPS_counter *fpsCounter;
    Timer *timer;
    controls::text *fpsText;
    std::u16string fpsString;

    enum ActiveMode { Screens, Devices };
    ActiveMode activeMode;
    Translation *choseTrl = nullptr;

    XImage *plus;
    XImage *tablet;

    bool addWinOpened = false;
    NewDisplay *newDisplay;

public:

    DisplayChoice(XWindow *window, const std::vector<XDisplay *>& displays,
                  std::vector<Translation *> *Translations, Server *server, NewDisplay *newDisplay) {
        this->xWindow = window;
        this->xWindow->updateAttribs();
        this->newDisplay = newDisplay;

        glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);

        glEnable(GL_TEXTURE_2D);
        glShadeModel(GL_SMOOTH);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glClearColor((float)0x2f/255, (float)0x2f/255, (float)0x2f/255, 1.0f);

        this->Translations = Translations;
        this->server = server;

        this->interfaceContext = new InterfaceContext();
        interfaceContext->xWindow = window;

        this->interfaceContext->rectangleShader = new ShaderProgram((char*)"../Resources/Shaders/ImageVertexShader.sdr",
                                                                    (char*)"../Resources/Shaders/ImageFragmentShader.sdr");
        this->interfaceContext->rectangleShader->loadFromFile();

        this->interfaceContext->textShader = new ShaderProgram((char*)"../Resources/Shaders/TextVertexShader.sdr",
                                                               (char*)"../Resources/Shaders/TextFragmentShader.sdr");
        this->interfaceContext->textShader->loadFromFile();

        plus = LoadImage(this->xWindow->xDisplay->display, this->xWindow->xDisplay->visualInfo->visual, (char*)"../Resources/Textures/plus.png");
        tablet = LoadImage(this->xWindow->xDisplay->display, this->xWindow->xDisplay->visualInfo->visual, (char*)"../Resources/Textures/tablet.png");

        DisplayList = new ListPreview(interfaceContext, &previews, Strings::Screens.data());

        activeMode = Screens;

        serverString = u"Can't start server on port: " + to_u16string(std::to_string(server->getPort()).data());
        serverText = new controls::text(interfaceContext,
                                     floor(xWindow->attribs.width*0.1),
                                     floor(xWindow->attribs.width*0.01),
                                     serverString.data(),
                                     glm::vec3(0.9f, 0.2f, 0.2f),
                                     xWindow->attribs.height / 50, 1.f);

        timer = new Timer();
        fpsCounter = new FPS_counter;

        fpsText = new controls::text(interfaceContext,
                                     floor(xWindow->attribs.width*0.01),
                                     floor(xWindow->attribs.width*0.01),
                                     const_cast<char16_t *>(u"fps: "),
                                     glm::vec3(0.7f, 0.7f, 0.7f),
                                     xWindow->attribs.height / 50, 1.f);

        timer->Reset();
        update();
    }

    void update(){
        interfaceContext->click.clicked = false;
        while (XCheckWindowEvent(xWindow->xDisplay->display, xWindow->window, xWindow->attribs.your_event_mask, &xWindow->event)) {

            switch ( xWindow->event.type ) {
                case Expose : {

                    if (xWindow->event.xexpose.count != 0)
                        break;

                    this->xWindow->updateAttribs();
                    glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);
                    glViewport(0, 0, this->xWindow->attribs.width, this->xWindow->attribs.height);
                    fpsText->update();
                    serverText->update();

                    XFlush(xWindow->xDisplay->display);
                    break;
                }
                case ButtonRelease: {
                    if(this->xWindow->event.xkey.keycode == 1){
                        interfaceContext->click.clicked = true;
                        interfaceContext->click.x = this->xWindow->event.xkey.x;
                        interfaceContext->click.y = this->xWindow->event.xkey.y;
                    }
                    break;
                }

            }
        }

        uint32_t tmp;
        Window w;
        XQueryPointer(xWindow->xDisplay->display,
                      xWindow->window, (Window*)(&tmp),
                      (Window*)(&tmp), (int*)(&tmp), (int*)(&tmp),
                      &interfaceContext->pointerPosition.x,
                      &interfaceContext->pointerPosition.y, &tmp);

        XGetInputFocus (xWindow->xDisplay->display, &w , (int*)(&tmp));
        interfaceContext->pointerOnWindow = xWindow->window == w;

        this->xWindow->xDisplay->updateScreen();

        if(addWinOpened) {
            newDisplay->show();
            addWinOpened = !newDisplay->update();
            newDisplay->redraw();
        }
        else newDisplay->hide();

        previews.clear();
        if(activeMode == Screens) {
            DisplayList->setHeader(Strings::Screens.data());
            for (int i = 0; i < Translations->size(); ++i) {
                if ((*Translations)[i]->isAvailable()) {
                    previews.emplace_back((*Translations)[i]->capture->image, (*Translations)[i]->name, i);
                }
            }

            previews.emplace_back(plus, Strings::Add, Translations->size());

        }
        else if (activeMode == Devices) {
            DisplayList->setHeader(Strings::Devices.data());
            auto clients = &(server->Clients);
            for (int i = 0; i < clients->size(); ++i) {
                    previews.emplace_back(tablet, to_u16string((*clients)[i]->name), i);
            }
        }

    }

    void redraw(){
        xWindow->updateAttribs();
        glXMakeCurrent(this->xWindow->xDisplay->display, this->xWindow->window, this->xWindow->context);
        glClear(GL_COLOR_BUFFER_BIT);

        DisplayList->update();
        int ret = DisplayList->redraw();
        if(ret >= 0){
            if(activeMode == Screens){
                if(ret == Translations->size()) {
                    addWinOpened = true;
                }
                else {
                    choseTrl = (*Translations)[ret];
                    activeMode = Devices;
                }
            }
            else if(activeMode == Devices){
                server->Clients[ret]->start((char*)"mpeg", choseTrl);
                activeMode = Screens;
            }
        }

        fpsString = u"fps: " + (std::u16string)to_u16string(fpsCounter->currentFPS);
        fpsText->caption = fpsString.data();
        fpsText->redraw();

        if(server->getError() > 0){
            serverString = u"Can't start server on port: " +
                    to_u16string(std::to_string(server->getPort()).data());
            serverText->caption = serverString.data();
            serverText->redraw();
        }
        else if(server->getError() == -1){
            serverString = u"Server closed";
            serverText->caption = serverString.data();
            serverText->redraw();
        }

        glFlush();
        glXSwapBuffers(this->xWindow->xDisplay->display, this->xWindow->window);
        XFlush(this->xWindow->xDisplay->display);
        fpsCounter->cutoff(timer->countDiff());
    }

    void clear() {
        release_element(interfaceContext)

        DisplayList->clear();
        release_element(DisplayList)

        release_element(fpsCounter)
        release_element(timer)
        fpsText->clear();
        release_element(fpsText)

        XDestroyImage(plus);
        XDestroyImage(tablet);
    }
};

#endif //STREAM_SCREEN_INTERFACE_H
