#ifndef OPENGL_LESSONS_FIGURE_CLASS_H
#define OPENGL_LESSONS_FIGURE_CLASS_H

#include <GL/gl.h>

class Figure{
private:
    //Buffers
    GLuint VAO;
    GLuint vertVBO;
    GLuint posVBO;
    GLuint colVBO;
    GLuint EBO;
    GLuint shaderParam;
    GLint locParam;

public:
    GLfloat *vertices;
    GLfloat *texPositions;
    GLfloat *colors;
    GLuint *indices;
    int vertexCount, indexCount;
    GLuint texture;
    ShaderProgram *shader;
    bool enableTexture = false;
    bool enableColor = false;

    void InitBuffers(){
        //Init buffers
        glGenVertexArrays(1, &this->VAO);
        glGenBuffers(1, &this->EBO);

        glBindVertexArray(this->VAO);
        // 2. Копируем вершины в буфер для OpenGL
        glGenBuffers(1, &this->vertVBO);
        glBindBuffer(GL_ARRAY_BUFFER, this->vertVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*this->vertexCount * 2, this->vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 2, GL_FLOAT,GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(0);

        shaderParam = 0;

        if(enableTexture) {
            shaderParam = shaderParam|0x1;
            glGenBuffers(1, &this->posVBO);
            glBindBuffer(GL_ARRAY_BUFFER, this->posVBO);
            glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * this->vertexCount * 2, this->texPositions, GL_STATIC_DRAW);
            glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid *) 0);
            glEnableVertexAttribArray(1);
        }

        if(enableColor) {
            shaderParam = shaderParam|0x2;
            glGenBuffers(1, &this->colVBO);
            glBindBuffer(GL_ARRAY_BUFFER, this->colVBO);
            glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * this->vertexCount * 4, this->colors, GL_STATIC_DRAW);
            glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid *) 0);
            glEnableVertexAttribArray(2);
        }

        // 3. Копируем индексы в в буфер для OpenGL
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*this->indexCount, this->indices, GL_STATIC_DRAW);

        // 4. Отвязываем VAO (НЕ EBO)
        glBindVertexArray(0);

        locParam = glGetUniformLocation(shader->ProgramID, "param");
    }

    void updateVertices(){
        glBindVertexArray(this->VAO);
        glBindBuffer(GL_ARRAY_BUFFER, this->vertVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*this->vertexCount * 2, this->vertices, GL_STATIC_DRAW);
        glBindVertexArray(0);
    }

    void updateTexPositions(){
        glBindVertexArray(this->VAO);
        glBindBuffer(GL_ARRAY_BUFFER, this->posVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * this->vertexCount * 2, this->texPositions, GL_STATIC_DRAW);
        glBindVertexArray(0);
    }

    void updateColors(){
        glBindVertexArray(this->VAO);
        glBindBuffer(GL_ARRAY_BUFFER, this->colVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * this->vertexCount * 4, this->colors, GL_STATIC_DRAW);
        glBindVertexArray(0);
    }

    void drawFigure(){
        glUniform1ui(locParam, shaderParam);
        glBindTexture(GL_TEXTURE_2D,this->texture);
        glBindVertexArray(this->VAO);
        glDrawElements(GL_TRIANGLES, this->indexCount, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    }

    void CleanBuffers(){
        glDeleteVertexArrays(1, &this->VAO);
        glDeleteBuffers(1, &this->vertVBO);
        glDeleteBuffers(1, &this->posVBO);
        glDeleteBuffers(1, &this->colVBO);
        glDeleteBuffers(1, &this->EBO);
    }
};

#endif //OPENGL_LASSONS_FIGURE_CLASS_H
