#ifndef STREAM_SCREEN_CONTROLS_H
#define STREAM_SCREEN_CONTROLS_H

#include "../Engine/XWindow.h"
#include "../Engine/Text.h"
#include "../Engine/Texture.h"
#include "Figure_class.h"
#include "../Engine/Utils.h"
#include "Strings.h"

#define release_element(x) if(x != nullptr) {delete x; x = nullptr;}
#define release_array(x) if(x != nullptr) {delete [] x; x = nullptr;}

struct InterfaceContext{
    XWindow *xWindow;
    ShaderProgram *textShader;
    ShaderProgram *rectangleShader;
    struct { int x = 0, y = 0; bool pressed = false; bool clicked = false;} click;
    struct { uint32_t code; char symbol; bool pressed = false;} keyPressed;
    struct PointerPosition{ int x = 0, y = 0;} pointerPosition;
    bool pointerSelectMode = true;
    bool pointerOnWindow = false;
};

namespace controls {

    class text {
    private:
        size_t size;
        InterfaceContext *interfaceContext;
        TextRenderer *tr;

    public:
        GLfloat x;
        GLfloat y;

        char16_t *caption;
        glm::vec3 color;
        float scale;

        text(InterfaceContext *interfaceContext, GLfloat x, GLfloat y, char16_t *caption, glm::vec3 color, size_t size,
             float scale) {
            this->x = x;
            this->y = y;
            this->caption = caption;
            this->color = color;
            this->size = size;
            this->scale = scale;

            this->interfaceContext = interfaceContext;

            tr = new TextRenderer((char*)"../Resources/Fonts/DroidSansFallback.ttf", 1104, size);

            update();
        }

        GLfloat lineWidth(char16_t *text, GLfloat _scale) {
            return tr->lineWidth(text, _scale);
        }

        GLfloat lineHeight(char16_t *text, GLfloat _scale) {
            return tr->lineHeight(text, _scale);
        }

        GLfloat getSize(){ return size; }

        void update() {
            this->tr->setProjection(
                    glm::ortho(0.0f, static_cast<GLfloat>(this->interfaceContext->xWindow->attribs.width),
                               0.0f, static_cast<GLfloat>(this->interfaceContext->xWindow->attribs.height)));
        }

        void redraw() {
            glXMakeCurrent(this->interfaceContext->xWindow->xDisplay->display, this->interfaceContext->xWindow->window,
                           this->interfaceContext->xWindow->context);
            this->tr->RenderText(this->interfaceContext->textShader, caption, x, y, scale, color);
        }

        void clear(){
            if(tr != nullptr){
                tr->clear();
                delete tr;
            }
        }

    };

    class rectangle {
    private:
        InterfaceContext *interfaceContext;
        Figure *figure;
        glm::mat4 projection;

        rectangle(InterfaceContext *interfaceContext, GLfloat x, GLfloat y, GLfloat width, GLfloat height) {
            this->x = x;
            this->y = y;
            this->width = width;
            this->height = height;

            this->interfaceContext = interfaceContext;

            this->figure = new Figure();

            this->figure->shader = this->interfaceContext->rectangleShader;

            this->figure->vertices = new GLfloat[8];
            this->figure->texPositions = new GLfloat[8];

            this->figure->vertexCount = 4;

            this->figure->texPositions[0] = 0.f;
            this->figure->texPositions[1] = 0.f;
            this->figure->texPositions[2] = 0.f;
            this->figure->texPositions[3] = 1.f;
            this->figure->texPositions[4] = 1.f;
            this->figure->texPositions[5] = 1.f;
            this->figure->texPositions[6] = 1.f;
            this->figure->texPositions[7] = 0.f;

            this->figure->indices = new GLuint[6]{
                    0, 1, 2,
                    0, 2, 3};
            this->figure->indexCount = 6;
        }

    public:
        GLfloat x;
        GLfloat y;
        GLfloat width;
        GLfloat height;
        glm::vec4 color;
        float *borderColor;
        XImage *xImage;

        rectangle(InterfaceContext *interfaceContext, GLfloat x, GLfloat y, GLfloat width, GLfloat height,
                  glm::vec4 color): rectangle(interfaceContext, x, y, width, height) {
            this->color = color;
            this->figure->colors = new float[16]{
                    color[0], color[1], color[2], 1.f,
                    color[0], color[1], color[2], 1.f,
                    color[0], color[1], color[2], 1.f,
                    color[0], color[1], color[2], 1.f
            };
            figure->enableColor = true;

            update();
            figure->InitBuffers();
        }

        rectangle(InterfaceContext *interfaceContext, GLfloat x, GLfloat y, GLfloat width, GLfloat height, XImage *xImage): rectangle(interfaceContext, x, y, width, height) {
            this->xImage = xImage;

            figure->enableTexture = true;
            borderColor = new float[4]{1.0f, 0.0f, 0.0f, 1.0f};

            glGenTextures(1, &this->figure->texture);
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

            glBindTexture(GL_TEXTURE_2D, this->figure->texture);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            glBindTexture(GL_TEXTURE_2D, 0);

            figure->InitBuffers();
        }

        void update() {
            glXMakeCurrent(this->interfaceContext->xWindow->xDisplay->display, this->interfaceContext->xWindow->window,
                           this->interfaceContext->xWindow->context);

            float temp;
            bool updateVertices = false;
            for (int i = 0; i < 8; i += 2) {
                temp = x + this->figure->texPositions[i] * this->width;
                if(this->figure->vertices[i] != temp){
                    updateVertices = true;
                    this->figure->vertices[i] = temp;
                }

                temp = y + (1 - this->figure->texPositions[i + 1]) * this->height;
                if(this->figure->vertices[i + 1] != temp){
                    updateVertices = true;
                    this->figure->vertices[i + 1] = temp;
                }
            }

            if(updateVertices) this->figure->updateVertices();

            if(figure->enableColor){
                for (int i = 0; i < 16; i += 4) {
                    for (int j = 0; j < 4; ++j) {
                        this->figure->colors[i + j] = color[j];
                    }
                }

                figure->updateColors();
            }

            if(figure->enableTexture){
                glBindTexture(GL_TEXTURE_2D, this->figure->texture);
                glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->xImage->width, this->xImage->height, 0, GL_BGRA,
                             GL_UNSIGNED_BYTE, this->xImage->data);
                glGenerateMipmap(GL_TEXTURE_2D);
            }

            this->projection = glm::ortho(0.0f, static_cast<GLfloat>(this->interfaceContext->xWindow->attribs.width),
                                          0.0f, static_cast<GLfloat>(this->interfaceContext->xWindow->attribs.height));
        }

        void redraw() {
            glXMakeCurrent(this->interfaceContext->xWindow->xDisplay->display, this->interfaceContext->xWindow->window,
                           this->interfaceContext->xWindow->context);
            interfaceContext->rectangleShader->Use();
            glUniformMatrix4fv(glGetUniformLocation(interfaceContext->rectangleShader->ProgramID, "projection"), 1,
                               GL_FALSE, glm::value_ptr(projection));
            figure->drawFigure();
        }

        void clear(){
            release_array(figure->vertices);
            release_array(figure->texPositions);
            release_array(figure->indices);
            release_array(figure->colors);
            release_array(borderColor);
        }

    };

    class button {
    private:
        uint32_t width, height, x, y;
        rectangle *background;
        text *caption;
        InterfaceContext *interfaceContext;
        bool focused = false;

    public:
        std::u16string caption_value;

        button(InterfaceContext *interfaceContext, uint32_t width, uint32_t height,
               uint32_t x, uint32_t y, char16_t *caption) {

            this->interfaceContext = interfaceContext;

            this->x = x;
            this->y = y;
            this->width = width;
            this->height = height;

            background = new rectangle(interfaceContext, x, y, width, height,
                                       glm::vec4(0.3f, 0.3f, 0.3f, 1.0f));

            caption_value = caption;
            this->caption = new text(interfaceContext, x, y, caption_value.data(), glm::vec3(0.9f, 0.9f, 0.9f),
                                     ((GLfloat) height *0.8f), 1.0f);
        }

        bool update() {

            caption->scale = (0.6f * height) / caption->lineHeight(caption_value.data(), 1.0f);

            uint32_t tmp = caption->lineWidth(caption_value.data(), caption->scale) / 2;
            caption->x = (uint32_t)(x + width / 2 - tmp);

            tmp = caption->lineHeight(caption_value.data(), caption->scale) / 2;
            caption->y = (uint32_t)(y + height / 2 - tmp);

            focused= false;
            if(interfaceContext->pointerOnWindow) {
                if (interfaceContext->pointerSelectMode) {
                    focused = PointInArea(x, y, width, height, interfaceContext->pointerPosition.x,
                                          interfaceContext->xWindow->attribs.height -
                                          interfaceContext->pointerPosition.y);
                }
            }

            if (focused) {
                if (interfaceContext->click.pressed)
                    background->color = glm::vec4(0.25f, 0.25f, 0.25f, 1.0f);
                else
                    background->color = glm::vec4(0.4f, 0.4f, 0.4f, 1.0f);
            }
            else {
                background->color = glm::vec4(0.3f, 0.3f, 0.3f, 1.0f);
            }



            if (interfaceContext->click.clicked) {
                if(PointInArea(x, y, width, height, interfaceContext->click.x, interfaceContext->xWindow->attribs.height - interfaceContext->click.y)){
                    return true;
                }
            }
            return false;
        }

        void redraw() {
            background->update();
            background->redraw();
            caption->update();
            caption->redraw();
        }
    };

    class textBox{
    private:
        rectangle *background;
        rectangle *pointer;
        text *value;
        bool focused = false;
        Timer *timer;
        uint32_t cursorPos = 0;
        InterfaceContext *interfaceContext;
        bool show_ptr = false;

    public:
        std::u16string val_str;
        uint32_t width, height, x, y;
        uint32_t max_len = 5;

        textBox(InterfaceContext *interfaceContext, uint32_t width, uint32_t height,
                uint32_t x, uint32_t y) {
            this->interfaceContext = interfaceContext;
            background = new rectangle(interfaceContext, x, y, width, height, glm::vec4(0.3f, 0.3f, 0.3f, 1.0f));
            pointer = new rectangle(interfaceContext, x + 1, y + height * 0.1f, 1, height * 0.8f, glm::vec4(0.9f, 0.9f, 0.9f, 1.0f));
            val_str = u"";
            value = new text(interfaceContext, (int)(x + height * 0.3f), y, val_str.data(), glm::vec3(0.9f, 0.9f, 0.9f), height * 0.9f, 1.0f);
            timer = new Timer();
            timer->Reset();

            this->x = x;
            this->y = y;
            this->width = width;
            this->height = height;
        }

        void update() {
            if (interfaceContext->click.clicked) {
                if(PointInArea(x, y, width, height, interfaceContext->click.x, interfaceContext->xWindow->attribs.height - interfaceContext->click.y)){
                    focused = true;
                    int l = val_str.size() + 1;
                    int index = 0, delta = interfaceContext->click.x - value->x, mn = delta;
                    for (int i = 0; i < l; ++i) {
                        int tmp = value->lineWidth(val_str.substr(0, i).data(), 1.0f);
                        if(abs(delta - tmp) < mn) {
                            index = i;
                            mn = abs(delta - tmp);
                        }
                    }
                    cursorPos = index;
                    timer->Reset();
                    show_ptr = true;
                }
                else {
                    focused = false;
                }
            }

            value->x = (int)(x + height * 0.3f);
            value->y = (int)(y + (height>>1) - value->lineHeight(val_str.data(), 1.0f) / 2);

            background->x = x;
            background->y = y;
            background->width = width;
            background->height = height;

            if(focused) {
                //printf("%d\n", interfaceContext->keyPressed.code);
                if (interfaceContext->keyPressed.pressed) {
                    if((interfaceContext->keyPressed.symbol >= '0') && (interfaceContext->keyPressed.symbol <= '9')){
                        if(val_str.size() < max_len) {
                            val_str = val_str.substr(0, cursorPos) +
                                      to_u16string(std::string(1, interfaceContext->keyPressed.symbol).data()) +
                                      val_str.substr(cursorPos, val_str.size() - cursorPos);
                            value->caption = val_str.data();
                            cursorPos++;
                        }
                        timer->Reset();
                        show_ptr = true;
                    }
                    else if (interfaceContext->keyPressed.code == 22) {
                        if(cursorPos > 0) {
                            val_str = val_str.substr(0, cursorPos - 1) +
                                      val_str.substr(cursorPos, val_str.size() - cursorPos);
                            value->caption = val_str.data();
                            cursorPos--;
                            timer->Reset();
                            show_ptr = true;
                        }
                    }
                    else if (interfaceContext->keyPressed.code == 119) {
                        if(cursorPos < val_str.size()) {
                            val_str = val_str.substr(0, cursorPos) +
                                      val_str.substr(cursorPos + 1, val_str.size() - cursorPos);
                            value->caption = val_str.data();
                            timer->Reset();
                            show_ptr = true;
                        }
                    }
                    else if (interfaceContext->keyPressed.code == 113) {
                        if(cursorPos > 0) {
                            cursorPos--;
                            timer->Reset();
                            show_ptr = true;
                        }
                    }
                    else if (interfaceContext->keyPressed.code == 114) {
                        if(cursorPos < val_str.size()) {
                            cursorPos++;
                            timer->Reset();
                            show_ptr = true;
                        }
                    }
                }
                if(timer->countDiff_no_reset() > 5e8) {
                    timer->Reset();
                    show_ptr = !show_ptr;
                }
            }
            else {
                show_ptr = false;
            }

        }

        void redraw() {
            background->update();
            background->redraw();
            value->update();
            value->redraw();

            if(show_ptr) {
                pointer->x = value->lineWidth(val_str.substr(0,  cursorPos).data(), 1.0f) + value->x;
                pointer->update();
                pointer->redraw();
            }
        }
    };

    class screenPreview {
    private:
        InterfaceContext *interfaceContext;

        rectangle *background;
        rectangle *preview;
        std::u16string caption_value;
        text *caption;
        bool focused = false;

        uint32_t oldWidth, oldHeight, oldX, oldY;
        float previewWidth = 0, previewHeight = 0, previewXScale = 0.9f, previewYScale = 0.65f;

    public:
        XImage *image;
        uint32_t width, height, x, y;

        screenPreview(XImage *image, InterfaceContext *interfaceContext, uint32_t width, uint32_t height,
                      uint32_t x, uint32_t y, char16_t *caption) {
            this->image = image;
            this->interfaceContext = interfaceContext;

            this->x = x;
            this->y = y;
            this->width = width;
            this->height = height;

            caption_value = caption;

            background = new rectangle(interfaceContext, 0, 0, 0, 0,
                                       glm::vec4(0.3f, 0.3f, 0.3f, 1.0f));

            preview = new rectangle(interfaceContext, 0, 0, 0, 0, image);

            this->caption = new text(interfaceContext, 0, 0, caption_value.data(), glm::vec3(0.9f, 0.9f, 0.9f),
                                     ((GLfloat) height / 10), 1.0f);

            //updateSize();
        }

        void setText(char16_t *new_caption){
            caption_value = new_caption;
            caption->caption = caption_value.data();
        }

        void updateSize(){
            background->x = x;
            background->y = y;
            background->width = width;
            background->height = height;

            previewWidth = width * previewXScale;
            previewHeight = height * previewYScale;

            float scale = ScaleArea(previewWidth, previewHeight, image->width, image->height);
            previewWidth = image->width * scale;
            previewHeight = image->height * scale;

            preview->x = (GLfloat)x + ((GLfloat)width-previewWidth)*0.5f;
            preview->y = (GLfloat)y + (GLfloat)height*0.575f - previewHeight*0.5f;
            preview->width = image->width*scale;
            preview->height = image->height*scale;

            float lh = this->caption->lineHeight(caption_value.data(), 1.0f);
            float scl = 1.f;
            if (lh > height * 0.2f) {
                this->caption->scale = height * 0.2f / lh;
                scl = this->caption->scale;
            }

            float tmp = this->caption->lineWidth(caption_value.data(), scl);
            if (tmp > width * 0.4f) {
                this->caption->scale = scl * width * 0.4f / tmp;
                tmp = width * 0.4f * scl;
            }
            this->caption->x = (GLint)(x + (float) width / 2 - tmp / 2);
            this->caption->y = (GLint)(y + (GLfloat) height / 8 - this->caption->lineHeight(caption_value.data(), this->caption->scale)*0.5f);
        }

        bool update() {

            if((oldX != x)||(oldY != y)||(oldWidth != width)||(oldHeight != height)) {
                oldX = x;
                oldY = y;
                oldWidth = width;
                oldHeight = height;
                updateSize();
            }

            focused= false;
            if(interfaceContext->pointerOnWindow) {
                if (interfaceContext->pointerSelectMode) {
                    focused = PointInArea(x, y, width, height, interfaceContext->pointerPosition.x,
                                          interfaceContext->xWindow->attribs.height -
                                          interfaceContext->pointerPosition.y);
                }
            }

            if (focused) {
                background->color = glm::vec4(0.4f, 0.4f, 0.4f, 1.0f);
            }
            else {
                background->color = glm::vec4(0.3f, 0.3f, 0.3f, 1.0f);
            }

            preview->xImage = image;

            if (interfaceContext->click.clicked) {
                if(PointInArea(x, y, width, height, interfaceContext->click.x, interfaceContext->xWindow->attribs.height - interfaceContext->click.y)){
                    return true;
                }
            }
            return false;
        }

        void redraw() {
            background->update();
            background->redraw();
            //previewBackground->update();
            //previewBackground->redraw();
            preview->update();
            preview->redraw();
            caption->update();
            caption->redraw();
        }

        void clear(){
            background->clear();
            release_element(background);
            preview->clear();
            release_element(preview);
            caption->clear();
            release_element(caption);
        }
    };
}

#endif //STREAM_SCREEN_CONTROLS_H
